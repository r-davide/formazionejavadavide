package it.synclab.abstractClass.es1;

public class TestOggetto3D {

	public static void main(String[] args) {
		double area = 30.55; 
		double altezza = 10.42; 
		
		Oggetto3D obj = new Oggetto3D(area, altezza); 
		obj.printVolume();
		
	}
}
