/*
 * Realizzare un programma che sia in grado di valutare il volume di oggetti tridimensionali come cilindri 
 * e parallelepipedi basandosi sul valore della loro area e dell&#39;altezza. Delegare i metodi comuni, 
 * come il calcolo e la stampa del volume ad una classe astratta.
 */

package it.synclab.abstractClass.es1;

public class Oggetto3D extends VolumeOggetto3D {

	private double area, altezza;

	public Oggetto3D(double area, double altezza) {
		setArea(area);
		setAltezza(altezza);
		calcolaVolume(area, altezza);
	}

	public void setArea(double area) {
		this.area = area;
	}

	public void setAltezza(double altezza) {
		this.altezza = altezza; 
	}

	public double getArea() {
		return this.area;
	}

	public double getAltezza() {
		return this.altezza;
	}
}
