/*
 * Realizzare un programma che sia in grado di valutare il volume di oggetti tridimensionali come cilindri 
 * e parallelepipedi basandosi sul valore della loro area e dell&#39;altezza. Delegare i metodi comuni, 
 * come il calcolo e la stampa del volume ad una classe astratta.
 */

package it.synclab.abstractClass.es1;

public abstract class VolumeOggetto3D {
	private double volume;

	protected double calcolaVolume(double area, double altezza) {
		volume = area * altezza;
		return volume;
	}

	public void printVolume() {
		if (volume == 0) {
			System.out.println("Uno dei parametri inseriti per il calcolo è pari a 0.");
		} else {
			System.out.println("Il volume dell'oggetto e': \n" + volume);
		}
	}
	
	public double getVolume() {
		return volume;
	}
}
