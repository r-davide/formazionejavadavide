/*
 * Scrivere un programma che simuli il lancio di un dado e di una moneta stampandone il risultato; 
 * con e senza l'utilizzo di una classe astratta che rappresenti i comportamenti comuni degli oggetti 
 * dado e moneta istanziati.
 */

package it.synclab.abstractClass.es2;

import java.util.Random;

public class DadoAbs extends OggettoLanciatoAbs {
	private int numeroFacce;

	public DadoAbs(int numeroFacce) {
		setNumeroFacce(numeroFacce);
	}

	public void lancia() {
		Random sorteggio = new Random();
		// aggiungo 1 al sorteggio poichè l'intervallo considerato sarebbe 0-5
		this.esito = 1 + sorteggio.nextInt(this.getNumeroFacce());
	}

	public void setNumeroFacce(int numeroFacce) {
		this.numeroFacce = numeroFacce;

	}

	public int getNumeroFacce() {
		return numeroFacce;
	}

}
