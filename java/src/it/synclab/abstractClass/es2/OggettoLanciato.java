/*
 * Scrivere un programma che simuli il lancio di un dado e di una moneta stampandone il risultato; 
 * con e senza l'utilizzo di una classe astratta che rappresenti i comportamenti comuni degli oggetti 
 * dado e moneta istanziati.
 */

package it.synclab.abstractClass.es2;

import java.util.Random;

//classe padre che implementa il lancio di un oggetto con n possibili esiti
public class OggettoLanciato {
	private int esito;
	private Random random = new Random();
	protected int numeroFacce;

	public OggettoLanciato(int numeroFacce) {
		this.numeroFacce = numeroFacce;
	}
	
	//metodo che simula il lancio dell'oggetto
	public void lancia() {
		this.esito = 1 + random.nextInt(this.numeroFacce);
	}

	public void stampaEsito() {
		System.out.println("Esito: " + this.getEsito());
	}

	public int getNumeroFacce() {
		return numeroFacce;
	}

	public int getEsito() {
		return esito;
	}
}
