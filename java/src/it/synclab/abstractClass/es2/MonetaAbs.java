/*
 * Scrivere un programma che simuli il lancio di un dado e di una moneta stampandone il risultato; 
 * con e senza l'utilizzo di una classe astratta che rappresenti i comportamenti comuni degli oggetti 
 * dado e moneta istanziati.
 */

package it.synclab.abstractClass.es2;
import java.util.Random;

public class MonetaAbs extends OggettoLanciatoAbs {
	private final int FACCE = 2; // numero di facce costante
	
	public MonetaAbs() {} // costruttore vuoto, l'unica variabile di istanza è già inizializzata
	
	public void lancia() {
		Random testaCroce = new Random();
		this.esito = testaCroce.nextInt(FACCE);
	}
	
	//gli esiti possibili 0 (testa) oppure 1 (croce)
	public void stampaEsito() {
		if (this.getEsito() == 0) {
			System.out.println("Esito: testa");
		} else {
			System.out.println("Esito: croce");
		}
	}
}
