/*
 * Scrivere un programma che simuli il lancio di un dado e di una moneta stampandone il risultato; 
 * con e senza l'utilizzo di una classe astratta che rappresenti i comportamenti comuni degli oggetti 
 * dado e moneta istanziati.
 */

package it.synclab.abstractClass.es2;

public abstract class OggettoLanciatoAbs {
	protected int esito;
		
	//metodo astratto che simula il lancio dell'oggetto
	public abstract void lancia();
	
	public void stampaEsito() {
		System.out.println("Esito: " + getEsito());
	}
	
	public int getEsito() {
		return esito;
	}
	
}
