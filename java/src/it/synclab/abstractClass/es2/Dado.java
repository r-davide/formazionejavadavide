/*
 * Scrivere un programma che simuli il lancio di un dado e di una moneta stampandone il risultato; 
 * con e senza l'utilizzo di una classe astratta che rappresenti i comportamenti comuni degli oggetti 
 * dado e moneta istanziati.
 */

package it.synclab.abstractClass.es2;

public class Dado extends OggettoLanciato {

	public Dado(int numeroFacce) {
		super(numeroFacce);
	}

	public void setNumeroFacce(int numeroFacce) {
		this.numeroFacce = numeroFacce;
	}

}
