/*
 * Scrivere un programma che simuli il lancio di un dado e di una moneta stampandone il risultato; 
 * con e senza l'utilizzo di una classe astratta che rappresenti i comportamenti comuni degli oggetti 
 * dado e moneta istanziati.
 */

package it.synclab.abstractClass.es2;

public class TestLancio {

	public static void main(String[] args) {

		// test per le classi DadoAbs e MonetaAbs che estendono la classe
		// astratta OggettoLanciatoAbs
		DadoAbs d1 = new DadoAbs(4);
		if (d1.getNumeroFacce() < 4) {
			System.out.println("Inserire un numero di facce del dado maggiore o uguale a 4.");
			System.exit(0);
		} else {
			System.out.println("IMPLENTAZIONE USANDO UNA CLASSE ABSTRACT...");
			System.out.println("Simulazione di 3 lanci di un dado a 6 facce: ");
			for (int i = 0; i < 3; i++) {
				d1.lancia();
				System.out.print("Lancio numero " + (i + 1) + " > ");
				d1.stampaEsito();
			}
		}

		Moneta m1 = new Moneta();
		System.out.println("\nSimulazione di 3 lanci di una moneta: ");
		for (int i = 0; i < 3; i++) {
			m1.lancia();
			System.out.print("Lancio numero " + (i + 1) + " > ");
			m1.stampaEsito();
		}

		// test per le classi Dado e Moneta che estendono la classe
		// OggettoLanciato
		Dado d2 = new Dado(4);
		if (d1.getNumeroFacce() < 4) {
			System.out.println("Inserire un numero di facce del dado maggiore o uguale a 4.");
			System.exit(0);
		} else {
			System.out.println("\nIMPLENTAZIONE SENZA L'UTILIZZO DI ABSTRACT...");
			System.out.println("Simulazione di 3 lanci di un dado a 6 facce: ");
			for (int i = 0; i < 3; i++) {
				d2.lancia();
				System.out.print("Lancio numero " + (i + 1) + " > ");
				d2.stampaEsito();
			}
		}

		Moneta m2 = new Moneta();
		System.out.println("\nSimulazione di 3 lanci di una moneta: ");
		for (int i = 0; i < 3; i++) {
			m2.lancia();
			System.out.print("Lancio numero " + (i + 1) + " > ");
			m2.stampaEsito();
		}

	}
}
