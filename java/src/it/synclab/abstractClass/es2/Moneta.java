/*
 * Scrivere un programma che simuli il lancio di un dado e di una moneta stampandone il risultato; 
 * con e senza l'utilizzo di una classe astratta che rappresenti i comportamenti comuni degli oggetti 
 * dado e moneta istanziati.
 */

package it.synclab.abstractClass.es2;

public class Moneta extends OggettoLanciato {
	private static final int FACCE = 2; //il numero di facce di una moneta è costante

	public Moneta() { //non utilizza il costruttore della classe padre poichè il numero di facce non è variabile
		super(FACCE);
	}
	
	//gli esiti possibili sono 1 (testa) oppure 2 (croce)
	public void stampaEsito() {
		if (this.getEsito() == 1) 
			System.out.println("Esito: testa!");
		else 
			System.out.println("Esito: croce!");
	}

}
