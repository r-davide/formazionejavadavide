package it.synclab.interfaces.es3;

public class TestOperazioni {

	public void testAddizione() {
		Addizione a = new Addizione();
		a.op(5.64, 3.37);
		System.out.print("TEST ADDIZIONE. \n5.64 + 3.37 = ");
		a.printRisultato();

		System.out.println();
	}

	public void testMoltiplicazione() {
		Moltiplicazione m = new Moltiplicazione();
		m.op(5, 3);
		System.out.print("TEST MOLTIPLICAZIONE. \n5.0 * 3.0 = ");
		m.printRisultato();

		System.out.println();
	}

	public void testDivisione() {
		Divisione d = new Divisione();
		Divisione z = new Divisione();
		d.op(5, 3);
		z.op(8.0, 0);
		System.out.print("TEST DIVISIONE. \n5.0 / 0  = ");
		z.printRisultato();
		System.out.print("5.0 / 3.0  = ");
		d.printRisultato();

		System.out.println();
	}

	public static void main(String[] args) {
		TestOperazioni test = new TestOperazioni();

		test.testAddizione();
		test.testMoltiplicazione();
		test.testDivisione();
	}
}
