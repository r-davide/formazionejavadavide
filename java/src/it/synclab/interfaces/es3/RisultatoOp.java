/*
 * Definire una interfaccia "operazione" e le sue classi derivate( sub. div, mul) in modo che da ciascuna 
 * sia possibile eseguire la corrispondente operazione aritmetica (di addizione, divisione, moltiplicazione) 
 * istanziando un oggetto di classe "operazione" e due operandi. Realizzare anche, una classe astratta che 
 * implementi le funzionalità comuni come la stampa a video del risultato.
 */

package it.synclab.interfaces.es3;

public abstract class RisultatoOp {
	private double risultato;
	
	public void printRisultato() {
		System.out.println(this.getRisultato());
	}

	public double getRisultato() {
		return risultato;
	}

	public void setRisultato(double risultato) {
		this.risultato = risultato;
	}

}
