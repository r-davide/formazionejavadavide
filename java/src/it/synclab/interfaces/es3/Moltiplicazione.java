/*
 * Definire una interfaccia "operazione" e le sue classi derivate( sub. div, mul) in modo che da ciascuna 
 * sia possibile eseguire la corrispondente operazione aritmetica (di addizione, divisione, moltiplicazione) 
 * istanziando un oggetto di classe "operazione" e due operandi. Realizzare anche, una classe astratta che 
 * implementi le funzionalità comuni come la stampa a video del risultato.
 */

package it.synclab.interfaces.es3;

public class Moltiplicazione extends RisultatoOp implements Operazione {

	public void op(double num1, double num2) {
		double risultato = num1 * num2;
		this.setRisultato(risultato);
	}
}
