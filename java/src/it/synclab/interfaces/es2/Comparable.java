/*
 * Scrivere un programma che sia in grado di istanziare degli oggetti "distributori di benzina"; 
 * di cui sia nota la città, il proprietario, la capacità e la benzina attualmente contenuta nel distributore. 
 * Dell'oggetto Distributore, deve essere possibile simulare le operazioni di erogazione del carburante e dei 
 * corrispondenti incassi. Implementa una interfaccia Comparable, in modo da consentire il confronto tra 2 
 * distributori in base alla capacità del serbatoio di carburante.
 */

package it.synclab.interfaces.es2;

public interface Comparable {
	public double compareTo(Distributore d);
}
