/*
 * Scrivere un programma che sia in grado di istanziare degli oggetti "distributori di benzina"; 
 * di cui sia nota la città, il proprietario, la capacita e la benzina attualmente contenuta nel distributore. 
 * Dell'oggetto Distributore, deve essere possibile simulare le operazioni di erogazione del carburante e dei 
 * corrispondenti incassi. Implementa una interfaccia Comparable, in modo da consentire il confronto tra 2 
 * distributori in base alla capacita del serbatoio di carburante.
 */

package it.synclab.interfaces.es2;

public class Distributore implements Comparable {
	private String citta, proprietario;
	private Erogatore[] erogatore;
	private double totaleSpese, totaleGuadagno;
	private double prezzoAlLitro = 0.80, prezzoVenditaAlLitro = 1.50, importoRifornimento = 0.0;

	public Distributore(String citta, String proprietario, Erogatore[] erogatore) {
		super();
		this.citta = citta;
		this.proprietario = proprietario;
		this.erogatore = erogatore;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public Erogatore[] getErogatore() {
		return erogatore;
	}

	public void setErogatore(Erogatore[] erogatore) {
		this.erogatore = erogatore;
	}

	public double getPrezzoAlLitro() {
		return prezzoAlLitro;
	}

	public void setPrezzoAlLitro(double prezzoAlLitro) {
		this.prezzoAlLitro = prezzoAlLitro;
	}

	public double getPrezzoVenditaAlLitro() {
		return prezzoVenditaAlLitro;
	}

	public void setPrezzoVenditaAlLitro(double prezzoVenditaAlLitro) {
		this.prezzoVenditaAlLitro = prezzoVenditaAlLitro;
	}

	public double getTotaleGuadagno() {
		return totaleGuadagno;
	}

	public void setTotaleGuadagno(double totaleGuadagno) {
		this.totaleGuadagno = totaleGuadagno;
	}

	public double getTotaleSpese() {
		return totaleSpese;
	}

	public void setTotaleSpese(double totaleSpere) {
		this.totaleSpese = totaleSpere;
	}

	public String toString() {
		return "[citta = " + citta + ", proprietario = " + proprietario + ", prezzo = " + prezzoVenditaAlLitro
				+ " EU/litro" + "]";
	}

	public double compareTo(Distributore d) {
		double comp = this.getCapacita() - d.getCapacita();
		// se comp è negativo allora il chiamante ha capacita maggiore
		// se comp è positivo, viceversa
		return comp;
	}

	public double eroga(double litri) {
		double diff;
		boolean trovato = false;
		Erogatore e = new Erogatore();
		int j = 0;
		// cicla per trovare l'erogatore che ha i litri necessari
		while (!trovato && j < erogatore.length) {
			for (int i = 0; i < erogatore.length; i++) {
				if (erogatore[i].getLitriRimasti() >= litri) {
					e = erogatore[i];
					trovato = true;
				}
				j++;
			}
		}
		// se non lo trova nel ciclo precedente, vuol dire che nessun
		// erogatore ha carburante sufficiente, quindi non eroga
		if (trovato) {
			this.importoRifornimento = litri * this.prezzoVenditaAlLitro;
			diff = e.getLitriRimasti();
			diff -= litri;
			e.setLitriRimasti(diff);
			// calcolaGuadagno();
		} else {
			this.importoRifornimento = 0.0;
		}
		return importoRifornimento;
	}

	public double getCarburanteRimasto() {
		double totale = 0.0;
		for (int i = 0; i < erogatore.length; i++) {
			totale += erogatore[i].getLitriRimasti();
		}
		return totale;
	}

	public double getCapacita() {
		double capacita = 0.0;
		for (int i = 0; i < erogatore.length; i++) {
			capacita += erogatore[i].getCapienzaSerbatoio();
		}
		return capacita;
	}

	public double rifornimentoDistributore(double litri) {
		// spese per il rifornimento
		this.totaleSpese = litri * this.prezzoAlLitro;
		boolean finito = false;
		double diff;
		int j = 0;
		// rifornisce il primo erogatore che ha la capienza necessaraia
		while (!finito && j < erogatore.length) {
			for (int i = 0; i < erogatore.length; i++) {
				diff = erogatore[i].getCapienzaSerbatoio() - erogatore[i].getLitriRimasti();
				if (diff >= litri) {
					litri += erogatore[i].getLitriRimasti();
					erogatore[i].setLitriRimasti(litri);
					finito = true;
				}
			}
		}
		if (!finito) {
			j = 0;
			while (litri == 0.0 && j < erogatore.length) {
				diff = erogatore[j].getCapienzaSerbatoio() - erogatore[j].getLitriRimasti();
				if (litri < diff) {
					litri += erogatore[j].getLitriRimasti();
					erogatore[j].setLitriRimasti(litri);
					litri = 0.0;
				} else {
					litri -= diff;
					erogatore[j].setLitriRimasti(erogatore[j].getCapienzaSerbatoio());
				}
				j += 1;
			}
		}
		return this.totaleSpese;
	}
}
