package it.synclab.interfaces.es2;

import java.util.regex.Pattern;

public class Utility {
	
	public Utility() {}
	
	//controlla che la stringa contenga solo lettere e spazi
	//non permette di iniziare/terminare con uno spazio
	//non permette l'inserimento di numero e altri caratteri
	public boolean checkString(String str) {
		if (Pattern.matches("[^\\s][A-Za-z\\s]+[^\\s]", str)) {
			return true;
		} else {
			return false;
		}
	}
	
	//controlla che il tipo di carburante sia uno tra: 
	//benzina, diesel, metano, gpl
	public boolean checkCarburante(String str) {
		boolean cond = str.equalsIgnoreCase("benzina") 
				|| str.equalsIgnoreCase("diesel")
				|| str.equalsIgnoreCase("gpl")
				|| str.equalsIgnoreCase("metano");
		
		return cond;
	}

}
