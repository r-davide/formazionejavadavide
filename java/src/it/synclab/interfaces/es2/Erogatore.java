/*
 * Scrivere un programma che sia in grado di istanziare degli oggetti "distributori di benzina"; 
 * di cui sia nota la città, il proprietario, la capacita e la benzina attualmente contenuta nel distributore. 
 * Dell'oggetto Distributore, deve essere possibile simulare le operazioni di erogazione del carburante e dei 
 * corrispondenti incassi. Implementa una interfaccia Comparable, in modo da consentire il confronto tra 2 
 * distributori in base alla capacita del serbatoio di carburante.
 */

package it.synclab.interfaces.es2;

public class Erogatore {
	private String tipoCarburante;
	private double capienzaSerbatoio;
	private double litriRimasti;

	public Erogatore() {}

	public Erogatore(String tipoCarburante, double capienzaSerbatoio) {
		super();
		this.tipoCarburante = tipoCarburante;
		this.capienzaSerbatoio = capienzaSerbatoio;
		this.litriRimasti = capienzaSerbatoio;
	}

	public String getTipoCarburante() {
		return tipoCarburante;
	}

	public void setTipoCarburante(String tipoCarburante) {
		this.tipoCarburante = tipoCarburante;
	}

	public double getCapienzaSerbatoio() {
		return capienzaSerbatoio;
	}

	public void setCapienzaSerbatoio(double capienzaSerbatoio) {
		this.capienzaSerbatoio = capienzaSerbatoio;
	}

	public double getLitriRimasti() {
		return litriRimasti;
	}

	public void setLitriRimasti(double litriRimasti) {
		this.litriRimasti = litriRimasti;
	}

	// rifornisce il serbatorio di un erogatore di un distributore
	public void riempiSerbatoio(double rifornimento) {
		this.litriRimasti = litriRimasti + rifornimento;
	}

}
