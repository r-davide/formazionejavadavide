/*
 * Scrivere un programma che sia in grado di istanziare degli oggetti "distributori di benzina"; 
 * di cui sia nota la città, il proprietario, la capacita e la benzina attualmente contenuta nel distributore. 
 * Dell'oggetto Distributore, deve essere possibile simulare le operazioni di erogazione del carburante e dei 
 * corrispondenti incassi. Implementa una interfaccia Comparable, in modo da consentire il confronto tra 2 
 * distributori in base alla capacita del serbatoio di carburante.
 */

package it.synclab.interfaces.es2;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class TestDistributore {
	public static void main(String[] args) {
		Utility u = new Utility();
		NumberFormat nf = new DecimalFormat("0.00");
		Scanner k = new Scanner(System.in);

		Erogatore[] erogatoreQ8 = new Erogatore[2];
		Erogatore[] erogatoreEsso = new Erogatore[4];

		erogatoreQ8[0] = new Erogatore("benzina", 1000.00);
		erogatoreQ8[1] = new Erogatore("diesel", 1000.00);

		erogatoreEsso[0] = new Erogatore("Benzina", 1000.00);
		erogatoreEsso[1] = new Erogatore("Benzina", 2000.00);
		erogatoreEsso[2] = new Erogatore("Diesel", 1500.00);
		erogatoreEsso[3] = new Erogatore("diesel", 1500.00);

		// controllo degli input inseriti nel costruttore dell'erogatore
		for (int i = 0; i < erogatoreQ8.length; i++) {
			// metodo checkCarburante() della classe Utility
			if (!u.checkCarburante(erogatoreQ8[i].getTipoCarburante())) {
				System.out.println("ATTENZIONE, \"" + erogatoreQ8[i].getTipoCarburante()
						+ "\": Il tipo di carburante inserito non è valido.\n"
						+ "Inserire una scelta tra benzina, diesel, metano o gpl.\n");
				System.exit(0);
			}

			if (erogatoreQ8[i].getCapienzaSerbatoio() == 0) {
				System.out.println("La capienza iniziale degli erogatori non può esere 0.00;");
				System.exit(0);
			}
		}

		for (int i = 0; i < erogatoreEsso.length; i++) {
			// metodo checkCarburante() della classe Utility
			if (!u.checkCarburante(erogatoreEsso[i].getTipoCarburante())) {
				System.out.println("ATTENZIONE, \"" + erogatoreEsso[i].getTipoCarburante()
						+ "\": Il tipo di carburante inserito non è valido.\n"
						+ "Inserire una scelta tra benzina, diesel, metano o gpl.\n");
				System.exit(0);
			}

			if (erogatoreEsso[i].getCapienzaSerbatoio() == 0) {
				System.out.println("La capienza iniziale degli erogatori non può esere 0.00;");
				System.exit(0);
			}
		}

		Distributore q8 = new Distributore("Milano", "Mario Rossi", erogatoreQ8);
		Distributore esso = new Distributore("Torino", "Luca Bianchi", erogatoreEsso);

		// controllo degli input inseriti nel costruttore del distributore
		Distributore[] d = { q8, esso };

		for (int i = 0; i < d.length; i++) {
			// metodo checkString() della classe Utility
			if (!u.checkString(d[i].getCitta())) {
				System.out.println("ATTENZIONE, \"" + d[i].getCitta() + "\": La città inserita non è valida.\n"
						+ "La stringa non può essere vuota,\n" + "non può iniziare/terminare con uno spazio,\n"
						+ "non può contenere numeri.\n");
				System.exit(0);
			}
			if (!u.checkString(d[i].getProprietario())) {
				System.out.println("ATTENZIONE, \"" + d[i].getProprietario()
						+ "\": il nome del proprietario inserito non è valido.\n" + "La stringa non può essere vuota,\n"
						+ "non può iniziare/terminare con uno spazio,\n" + "non può contenere numeri.\n");
				System.exit(0);
			}

		}

		// l'utente sceglie quanle distributore utilizzare tra quelli
		// disponibili
		int input;
		;
		System.out.println("Distributore disponibili: ");
		for (int i = 0; i < d.length; i++) {
			System.out.println("Premi " + (i + 1) + " - " + d[i].toString());
		}

		// e viene indicato quale distributore contiene più carburante usando il
		// il metodo compareTo() implementato tramite interfaccia Comparable
		if (d[0].compareTo(d[1]) < 0) {
			System.out.println("\nIl distributore di " + d[0].getProprietario()
					+ " contiene meno carburante rispetto a quello di " + d[1].getProprietario());
		} else if (d[0].compareTo(d[1]) == 0) {
			System.out.println("\nI due distributore hanno la stessa quantità di carburante.");
		} else {
			System.out.println("\nIl distributore di " + d[0].getProprietario()
					+ " contiene più carburante rispetto a quello di " + d[1].getProprietario());
		}

		System.out.print("\nQual è la tua scelta? ");
		try {
			input = k.nextInt();
			if (input != 1 && input != 2) {
				System.out.print("Scelta non corretta, operazione terminata.");
				System.exit(0);
			}
			// viene mostrata all'utente la quantità di carburante per ogni
			// erogatore
			System.out.println();
			System.out.println("Benvenuto nel distributore di " + d[input - 1].getProprietario() + "!");
			System.out.println("La quantità di carburante è: ");
			Erogatore[] e = d[input - 1].getErogatore();

			for (int i = 0; i < e.length; i++) {
				System.out.println("Erogatore " + i + ": " + e[i].getCapienzaSerbatoio() + " litri");
			}
			System.out.println("Total: " + d[input - 1].getCapacita() + " litri\n");

			// richiesta di rifornimento
			String risposta;
			System.out.print("Vuoi effettuare rifornimento?\n" + "Rispondere \"si\" oppure \"no\":  ");
			risposta = k.next();

			if (!risposta.equalsIgnoreCase("si") && !risposta.equalsIgnoreCase("no")) {
				System.out.print("Inserimento non valido, operazione terminata. ");
				System.exit(0);

			} else {
				if (risposta.equalsIgnoreCase("si")) {
					double litri;
					System.out.print("Quanti litri? (premere 0 per terminare) ");
					litri = k.nextDouble();
					if (litri <= 0) {
						System.out.println("Operazione terminata.");
						System.exit(0);
					} else if (litri > d[input - 1].getCarburanteRimasto()) {
						System.out.println("La quantità eccede la disponibilità attuale." + " Operazione terminata.");
					} else {
						System.out.println("Rifornimento effettuato. Importo dovuto: "
								+ nf.format(d[input - 1].eroga(litri)) + " EU");
						System.out.println("\nCarburante rimasto al distributore: "
								+ d[input - 1].getCarburanteRimasto() + " litri");
					}

				} else {
					System.out.println("Operazione termianta.");
					System.exit(0);
				}
			}
			
		} catch (InputMismatchException ex) {
			System.out.println("ATTENZIONE: inserimento non valido.\nOperazione terminata.");
			System.exit(0);
		}

	}
}
