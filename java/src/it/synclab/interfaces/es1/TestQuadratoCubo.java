/*
 * Scrivere un programma che dato un numero intero in ingresso vengano restituiti un oggetto 
 * rappresentativo del quadrato del numero dato e un oggetto rappresentativo del cubo del numero assegnato. 
 * Implementa l'algoritmo attraverso l'uso di una interfaccia comune.
 */

package it.synclab.interfaces.es1;

public class TestQuadratoCubo {

	public void quadrato() {
		Quadrato q = new Quadrato();
		q.calcola(3);
		q.stampa();
	}
	
	public void cubo() {
		Cubo c = new Cubo(); 
		c.calcola(9);
		c.stampa();
	}
	
	public static void main(String[] args) {
		TestQuadratoCubo test = new TestQuadratoCubo();
		
		test.quadrato();
		test.cubo();
	}
}
