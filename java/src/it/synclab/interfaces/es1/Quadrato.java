/*
 * Scrivere un programma che dato un numero intero in ingresso vengano restituiti un oggetto 
 * rappresentativo del quadrato del numero dato e un oggetto rappresentativo del cubo del numero assegnato. 
 * Implementa l'algoritmo attraverso l'uso di una interfaccia comune.
 */

package it.synclab.interfaces.es1;

public class Quadrato implements QuadratoCubo {
	private int risultato;

	public void calcola(int numero) {
		this.risultato = numero*numero;
	}
	
	public void stampa() {
		int numero = (int)(Math.sqrt(this.getRisultato()));
		System.out.println("square(" + numero + ") = " + this.getRisultato());
	}

	public int getRisultato() {
		return this.risultato;
	}
}
