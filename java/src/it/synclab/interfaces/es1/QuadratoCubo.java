/*
 * Scrivere un programma che dato un numero intero in ingresso vengano restituiti un oggetto 
 * rappresentativo del quadrato del numero dato e un oggetto rappresentativo del cubo del numero assegnato. 
 * Implementa l'algoritmo attraverso l'uso di una interfaccia comune.
 */

package it.synclab.interfaces.es1;

public interface QuadratoCubo {
	public void calcola(int numero);
	public void stampa();
}
