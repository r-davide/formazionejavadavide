/*
 * Scrivere un programma che dato un numero intero in ingresso vengano restituiti un oggetto 
 * rappresentativo del quadrato del numero dato e un oggetto rappresentativo del cubo del numero assegnato. 
 * Implementa l'algoritmo attraverso l'uso di una interfaccia comune.
 */

package it.synclab.interfaces.es1;

public class Cubo implements QuadratoCubo {
	private int risultato;

	public void calcola(int numero) {
		this.risultato = numero * numero * numero;
	}

	public void stampa() {
		int numero = (int)(Math.cbrt(this.getRisultato()));
		System.out.println("cube(" + numero + ") = " + this.getRisultato());
	}

	public int getRisultato() {
		return this.risultato;
	}
}
