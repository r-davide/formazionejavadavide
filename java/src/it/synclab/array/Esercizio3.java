/*
 
 Scrivere un metodo che mostri i primi 50 numeri della serie di fibonacci (i primi due numeri di fibonacci
sono 0 e 1 i successivi si calcolano come somma dei 2 precedenti).

 */


package it.synclab.array;

public class Esercizio3 {
	
	public static void fibArr() {
		long[] arr = new long[50];
		
		//arr[0] = 0; di default
		arr[1] = 1; 
		for(int i = 2; i < arr.length; i++) {
			arr[i] = arr[i - 1] + arr[i - 2];
		}
		
		//stampa a video
		System.out.println("I primi 50 numeri della serie di Fibonacci: \n");
		for(int i = 0; i < arr.length; ++i) {
			System.out.println("fib(" + (i+1) + ") = " + arr[i]);
		}
	}

	public static void main(String[] args) {
		fibArr();
	}
}
