package it.synclab.array;
import java.util.Random;

public class EserciziArray1 {
	 
	/*
	 
	 Scrivere un programma / metodo che preveda un array di 10 numeri interi contenente valori 
	 a piacere e ne stampa gli elementi secondo il seguente ordine: il primo, l’ultimo, il secondo, 
	 il penultimo, il terzo, il terz’ultimo, ecc.
	 
	 */
	public static void printArr(int[] arr) {
		int i = 0;
		int j = arr.length - 1; 
		while (i < j) {
			System.out.println(arr[i] + " " + arr[j]);
			i++;
			j--;
		}
	}
	
	/*
	 
	 Scrivere un programma / metodo che preveda un array di 10 numeri interi contenente valori random.
	 Tale programma dovrà stampare la dicitura &quot;Pari e dispari uguali&quot; se la somma dei 
	 numeri in posizioni pari dell’array è uguale alla somma dei numeri in posizioni dispari, altrimenti
	 il programma dovrà stampare la dicitura &quot;Pari e dispari diversi&quot;.
	 
	 */
	
	public static void checkPariDispari(int[] arr) {
		int pari = 0, dispari = 0; 
		int n = arr.length;
		
		for (int i = 0; i < n; ++i) {
			if (i % 2 == 0) {
				pari += arr[i];
			} else {
				dispari += arr[i];
			}
		}
	
		if (pari == dispari) {
			System.out.println("Pari e dispari uguali.");
		} else {
			System.out.println("Pari e dispari diversi.");
		}
	}
	
	/*
	 
	 Scrivere un programma / metodo che preveda un array di 10 numeri interi contenente valori 
	 random e che stampi la dicitura "Tre valori consecutivi uguali" contiene tre valori 
	 uguali in tre posizioni consecutive,qualora la condizione non dovesse essere verificata dovrà
	 stampare "NO".
	 
	 */
	
	public static void checkTreConsecutivi(int[] arr) {
		int i = 0;
		boolean cond = false;
		while (cond == false && i < arr.length - 1) {
			if (arr[i] == arr[i + 1] && arr[i + 1] == arr[i + 2]) { 
				cond = true;
			}
			i++;
		}
		
		if (cond) {
			System.out.println("Tre valori consecutivi uguali.");
		} else {
			System.out.println("NO");
		}
	}

	/*
	 
	 Scrivere un programma / metodo che date due sequenze di stringhe, ciascuna di 5 elementi, 
	 stampi il messaggio "OK" se almeno una stringa della prima sequenza compare anche 
	 nella seconda, altrimenti sarà stampato il messaggio "KO". Qualora vengano trovate due 
	 stringhe uguali i confronti tra le sequenze devono essere interrotti.

	 */

	public static void checkString(String[] s1, String[] s2) {
		boolean finded = false;
		
		int i = 0;
		while (finded == false && i < s1.length) {
			for (int j = 0; j < s2.length; ++j) {
				if (s1[i].equals(s2[j])) finded = true;
			}	
			++i;
		}
		if (finded) {
			System.out.println("OK");
		} else {
			System.out.println("KO");
		}
	}
	
	/*
	 
	 Test dei metodi
	 
	 */
	
	public static void main(String[] args) {
		//test printArr()
		int[] arr1 = new int[10];
		for (int i = 0; i < arr1.length; ++i) {
			arr1[i] = i + 1;
		}
		System.out.println("Test metodo printArr(): ");
		printArr(arr1);
		
		//test pariDispari()
		Random r = new Random();
		int[] arr2 = new int[10];
		for (int i = 0; i < arr2.length; ++i) {
			arr2[i] = r.nextInt(100);
		}
		int[] arr3 = {3, 5, 2, 0, 42, 20, 2, 17, 1, 8};

		System.out.println("\nTest metodo pariDispari(): ");
		checkPariDispari(arr2); 
		checkPariDispari(arr3); //pari e dispari uguali
		
		//test checkTreValori()
		System.out.println("\nTest metodo checkTreConsecutivi(): ");
		int[] arr4 = {3, 5, 2, 0, 42, 42, 42, 21, 5, 2};
		checkTreConsecutivi(arr4);
		checkTreConsecutivi(arr2);
		
		//test checkString()
		System.out.println("\nTest metodo checkString(): ");
		String[] str1 = {"giallo", "rosso", "verde", "blu", "nero"};
		String[] str2 = {"bianco", "grigio", "giallo", "viola", "rosso"};
		String[] str3 = {"bianco", "grigio", "bar", "viola", "foo"};
		System.out.print("Confronto fra str1[] e str2[]: ");
		checkString(str1, str2);
		System.out.print("Confronto fra str1[] e str3[]: ");
		checkString(str1, str3);
	}

}
