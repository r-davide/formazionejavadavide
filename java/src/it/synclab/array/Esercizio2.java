/*
 
 Scrivere un metodo che verifichi se una data stringa inserita in input è palindroma, tale metodo dovrà
restituire un booleano.

 */

package it.synclab.array;

public class Esercizio2 {

	public static boolean checkPalindroma(String str) {
		boolean isPal = true;
		str = str.toLowerCase();
		if (str == null || str == "" || str.length() < 2) {
			System.out.println("Inserire una stringa la cui lunghezza è almeno pari a 2");
			System.exit(0);
		} else {
			int prev = 0;
			int next = str.length() - 1;
			while (prev < next) {
				if (str.charAt(prev) != str.charAt(next)) {
					isPal = false;
				}
				prev++;
				next--;
			}
			System.out.println("La parola \"" + str + "\" è palindroma? ");
		}
		return isPal;
	}

	public static void main(String[] args) {

		// test checkPalindorma();
		String str1 = "foo bar";
		String str2 = "Anna";
		String str3 = "oro";
		String str4 = "00422400";
		String str5 = "11235813";

		System.out.println(checkPalindroma(str1)); // false
		System.out.println(checkPalindroma(str2)); // true
		System.out.println(checkPalindroma(str3)); // true
		System.out.println(checkPalindroma(str4)); // true
		System.out.println(checkPalindroma(str5)); // false
	}

}
