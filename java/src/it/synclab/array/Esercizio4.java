/*
 
  Data una matrice effettuare la trasposta della stessa.
  
  Definizione trasposta : data una matrice A si dice trasposta una matrice At che abbia le colonne al posto
  delle righe e viceversa.
 
 */


package it.synclab.array;

public class Esercizio4 {
	
	public static void printMatrix(int[][] matrix) {
		for(int i = 0; i < matrix.length; ++i) {
			for(int j = 0; j < matrix[0].length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static int[][] trasposta(int[][] matrix) {
		int n = matrix.length;
		int m = matrix[0].length; //assumendo vi siano lo stesso numero di colonne per ogni riga
		
		int[][] matrixT = new int[m][n];
		for(int i = 0; i < n; ++i) {
			for(int j = 0; j < m; ++j) {
				matrixT[j][i] = matrix[i][j]; 
			}
			
		}
		
		return matrixT;
	}
	
	public static void main(String[] args) {
		int[][] matrix = {
				{2, 4, 8},
				{3, 2, 0},
				{5, 3, 1},
				{0, 1, 0}
		};
		
		int[][] t = trasposta(matrix);
		System.out.println("La matrice originale è: ");
		printMatrix(matrix);
		System.out.println("\nLa sua trasposta è: ");
		printMatrix(t);
	}
}
