package it.synclab.array;

public class EserciziStringhe2 {

	/*
	 * Scrivere un programma / metodo che data una stringa in input la stampi al
	 * contrario. Per esempio, se si immette la stringa "Viva Java", il
	 * programma stampa "avaJ aviV"
	 */

	public static void reverseString(String str) {
		if (str == null || str == "") {
			System.out.println("Inserimento non valido: stringa vuota.");
		} else {
			String reversed = "";
			System.out.print(str + " -> ");
			for (int i = str.length() - 1; i >= 0; --i) {
				reversed += str.charAt(i);
			}

			System.out.println(reversed);
		}
	}

	/*
	 * Scrivere un programma / metodo che data una stringa in input ne stampi le
	 * sole vocali. Per esempio, se si immette la stringa "Viva Java", il
	 * programma stampa "iaaa".
	 */

	public static void printVocali(String str) {
		if (str == null || str == "") {
			System.out.println("Inserimento non valido: stringa vuota.");
		} else {
			String vocali = "";
			for (int i = 0; i < str.length(); ++i) {
				if (str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o'
						|| str.charAt(i) == 'u') {
					vocali += str.charAt(i);
				}
			}
			System.out.print(str + " -> ");
			System.out.println(vocali);
		}
	}

	/*
	 * Scrivere un programma / metodo che data una sequenza di stringhe,
	 * conclusa dalla stringa vuota, stampi la somma delle lunghezze delle
	 * stringhe che iniziano con una lettera maiuscola. Per esempio, se si
	 * immettono le stringhe "Albero", "foglia", "Radici", "Ramo", "fiore" (e
	 * poi "" per finire), il programma stampa 16.
	 */

	public static void countString(String[] s) {
		int sum = 0;
		int i = 0;
		String str = "";
		while (s[i] != "" && s[i] != s[s.length - 1]) {
			if (Character.isUpperCase(s[i].charAt(0))) {
				str += s[i] + " ";
				sum += s[i].length();
			}
			i++;
		}
		System.out.println("Le stringhe considerate sono: " + str);
		System.out.println("La somma delle loro lunghezze e' " + sum);
	}

	/*
	 * Test dei metodi
	 */

	public static void main(String[] args) {
		System.out.println("Test reverseString(): ");
		System.out.print("test 1: ");
		reverseString("Foo bar");
		System.out.print("test 2: ");
		reverseString("");
		System.out.println("Test printVocali(): ");
		System.out.print("test 1: ");
		printVocali("Viva Java");
		System.out.print("test 2: ");
		printVocali("");

		String[] str = { "Albero", "foglia", "Radici", "Ramo", "fiore", "Pippo" , ""};
		for (int i = 0; i < str.length - 1; i++) {
			if (str[i] == null || str[i] == "") {
				System.out.println("Non è possibile inserire stringhe vuote, eccetto l'ultima.");
				System.exit(0);
			}
		}
		System.out.println("Test countString(): ");
		countString(str);
	}

}
