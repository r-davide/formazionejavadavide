/*
Dato un array di interi, popolato casualmente, ordinarlo in ordine crescente e stampare a video il risultato
di tale ordinamento.
*/

package it.synclab.array;
import java.util.Random;

public class Esercizio1 {
	
	//metodo per stampare un array di interi
	public static void printArr(int[] arr) {
		for(int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
	
	//implementazione insertion sort 
	public static int[] inSort(int[] arr) {
		//i parte da 1 poichè per definizione il primo elemento (indice 0) è già ordinato
		for(int i = 1; i < arr.length; ++i) {
			int k = arr[i];
			int j = i - 1;
			//controllo sulla validità dell'indice j + confronto fra la carta precedente e la successiva
			while (j >= 0 && arr[j] > k) {
				arr[j + 1] = arr[j];
				j = j - 1;
			}
			arr[j + 1] = k;
		}
		
		return arr;
	}

	public static void main(String[] args) {
		Random r = new Random();
		int[] arr = new int[10];
		
		//popolazione casuale dell'array considerando un intervallo di interi da 0 a 50.
		for(int i = 0; i < arr.length; i++) {
			arr[i] = r.nextInt(50); 
		}
		System.out.println("L'array prima dell'ordinamento:");
		printArr(arr);
		System.out.println("\nL'array dopo dell'ordinamento:");
		printArr(inSort(arr));
	}
}