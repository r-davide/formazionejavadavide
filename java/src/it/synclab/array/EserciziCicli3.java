package it.synclab.array;
import java.util.Scanner; 

public class EserciziCicli3 {

	/*
	 * Scrivere un programma / metodo che data una sequenza di interi stampi
	 * "Tutti positivi e pari" se i numeri inseriti sono tutti positivi e pari,
	 * altrimenti stampa "NO". Risolvere questo esercizio senza usare array.
	 */
	
	public static void positiveEven(String str) {
		boolean posEven = true;
	    String tmp = "";
	    int n = 0;
	    int i = 0;
	    
	    System.out.println("La stringa inserita è: " + str);
	    System.out.println("Contiene solo numeri positivi e pari?");
	    while (i < str.length() && posEven) {
	    	//set j to the next number available
	    	int j = i;
	      
	    	//store the first number inside tmp
	    	while (j < str.length() && Character.isDigit(str.charAt(j))) {
	    		tmp += str.charAt(j);
	    		j++;
	    	}
	    	
	    	//check if tmp is not empty
	    	if (tmp != "") {
	    		n = Integer.parseInt(tmp);
	    		if (n % 2 != 0 || n < 0) {
	    			posEven = false;
	    		}
	    	}
	    	//reset temporary string
	    	tmp = "";
	    	i = j;
	    	++i;
	    }

	    if (posEven) {
	    	System.out.println("Tutti positivi e pari.\n");
	    } else {
	    	System.out.println("NO.\n");
	    }
	}
	
	/*
	 * Scrivere un programma / metodo che data una sequenza di interi stampi la
	 * media di tutti i numeri inseriti che siano divisibili per tre. Per
	 * esempio, se si immettono i valori 5, 8, 9, 12, 7, 6 ,1 il risultato
	 * stampato dovrà essere 9. Risolvere questo esercizio senza usare array.
	 */

	public static void meanIfMod3(String str) {
		String tmp = "";
		int n = 0;
		int sum = 0;
		int counter = 0; 

		int i = 0;
		while (i < str.length()) {
			//set j to the next number available
			int j = i;

			//store the first number inside tmp
			while (j < str.length() && Character.isDigit(str.charAt(j))) {
				tmp += str.charAt(j);
				j++;
			}

			//check if tmp is not empty
			if (tmp != "") {
				n = Integer.parseInt(tmp);
				if (n % 3 == 0) {
					sum += n;
					counter++;
				}
			}
			//reset temporary string
			tmp = "";
			i = j;
			++i;
		}

		int mean = sum / counter; 
		System.out.println("Stringa in input: " + str);
		System.out.println("La media dei numeri divisibili per 3 è " + mean + "\n");

	}

	/*
	 * Scrivere un programma / metodo che chiede all’utente di inserire una
	 * sequenza di caratteri (chiedendo prima quanti caratteri voglia inserire)
	 * e li ristampa man mano che vengono inseriti. L’intero procedimento
	 * (chiedere quanti caratteri voglia inserire, leggere i caratteri e man
	 * mano stamparli) dovrà essere ripetuto 5 volte. Risolvere questo esercizio
	 * senza usare array.
	 */

	public static void keyboardInput() {
		Scanner kb = new Scanner(System.in);
		int n; 
		String input = "";
		int i = 0;
		do { 
			System.out.print("How many character do you wanna insert? ");
			do {
				n = kb.nextInt();
				if (n < 0) {
					System.out.println("Insert a number greater than 0: ");
				}
			} while (n < 0);

			for (int j = 0; j < n; ++j) {
				System.out.print("Insert a character: ");
				char c = kb.next().charAt(0);
				input += c;
				System.out.println("Your sequence: " + input);
			}
			input = "";
			i++;
		} while (i < 5);
	}

	
	/*
	 * Test
	 */

	public static void main(String[] args) {
		String str = "2 4 42 34 3 6";
		String str2 = "4 42 2 12";
		String str3 = "5, 8, 9, 12, 7, 6, 1";
		System.out.println("Test metodo positiveEven(): ");
		positiveEven(str); // NO.
		positiveEven(str2); // Tutti positivi e pari

		System.out.println("Test metodo meanIfMod3(): ");
		meanIfMod3(str3);
		meanIfMod3(str);

		System.out.println("Test metodo keyboardInput(): ");
		keyboardInput();

	}
}

