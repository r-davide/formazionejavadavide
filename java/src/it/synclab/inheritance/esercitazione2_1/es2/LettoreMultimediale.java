/*
 * Eseguire un oggetto multimediale significa invocarne il metodo show() se è un'immagine o il 
 * metodo play() se è riproducibile.

 * Organizzare opportunamente con classi astratte, interfacce e classi concrete il codice di un lettore
 * multimediale che memorizza 5 elementi (creati con valori letti da tastiera) in un array e poi chiede
 * ripetutamente all'utente quale oggetto eseguire (leggendo un intero da 1 a 5 oppure 0 per finire) e
 * dopo ogni esecuzione fornisce la possibilità di regolarne eventuali parametri (volume / luminosità).
 */

package it.synclab.inheritance.esercitazione2_1.es2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class LettoreMultimediale {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);

		// creazione array che memorizza 5 elementi multimediali: 2 audio, 2
		// immagini, 1 filmato
		Audio a1 = new Audio();
		Audio a2 = new Audio();
		Immagine im1 = new Immagine();
		Immagine im2 = new Immagine();
		Filmato f = new Filmato();

		ElementoMultimediale[] multimedia = { a1, a2, f, im1, im2 };
		System.out.println("Questo lettore multimediale contiene 5 elementi: 2 tracce audio, 1 filmato e 2 immagini.");
		// prende da input i dati per inizializzare gli elementi multimediali
		for (int i = 0; i < multimedia.length; i++) {
			String input;
			if (i == 0 || i == 1) { // audio
				// richiede e controlla input per la traccia audio
				System.out.print("Inserisci il titolo della traccia audio: ");
				input = kb.nextLine();
				if (input.isEmpty()) {
					System.out.println("Input errato. Il programma termina.");
					System.exit(0);
				} else {
					System.out.print("Inserisci la durata della traccia (valore intero positivo): ");
					try {
						int d = kb.nextInt();
						if (d > 0) {
							// inizializzo traccia audio con titolo e durata
							// presi in input e volume pari a 10 di default
							Audio audio = new Audio(input, d, 10);
							multimedia[i] = audio;
							// leggo la stringa vuota successiva non inserita
							// dall'utente; senza tale istruzione alla prossima
							// iterazione viene rilevata la stringa vuota
							input = kb.nextLine();
							System.out.println();
						} else {
							System.out.println(
									"E' richiesto un valore strettamente maggiore di 0.\n" + "Il programma termina.");
							System.exit(0);
						}
					} catch (InputMismatchException ex) {
						System.out.println(
								"ERRORE: è richiesto un valore numerico intero positivo.\n" + "Il programma termina.");
						System.exit(0);
					}
				}
			} else if (i == 2) { // filmato
				// richiede e controlla input per il filmato
				System.out.print("Inserisci il titolo del filmato: ");
				input = kb.nextLine();
				if (input.isEmpty()) {
					System.out.println("Input errato. Il programma termina.");
					System.exit(0);
				} else {
					System.out.print("Inserisci la durata del filmato (valore intero positivo): ");
					try {
						int d = kb.nextInt();
						if (d > 0) {
							// inizializzo traccia audio del filmato con la
							// stessa durata e volume di default a 10
							Audio audio = new Audio("audio-" + input, d, 10);
							// inizializzo filmato con titolo e durata presi in
							// input e luminosità pari a 5 di default
							Filmato film = new Filmato(input, d, audio, 5);
							multimedia[i] = film;
							// leggo la stringa vuota successiva non inserita
							// dall'utente; senza tale istruzione alla prossima
							// iterazione viene rilevata la stringa vuota
							input = kb.nextLine();
							System.out.println();
						} else {
							System.out.println(
									"E' richiesto un valore strettamente maggiore di 0.\n" + "Il programma termina.");
							System.exit(0);
						}
					} catch (InputMismatchException ex) {
						System.out.println(
								"ERRORE: è richiesto un valore numerico intero positivo.\n" + "Il programma termina.");
						System.exit(0);
					}
				}
			} else { // immagini
				// richiede e controlla input per l'immagine
				System.out.print("Inserisci il titolo dell'immagine: ");
				input = kb.nextLine();
				if (input.isEmpty()) {
					System.out.println("Input errato. Il programma termina.");
					System.exit(0);
				} else {
					System.out.print("Inserisci un valore intero positivo per la luminosita': ");
					try {
						int l = kb.nextInt();
						if (l > 0) {
							// inizializzo immagine con titolo e luminosita'
							// presi in input;
							Immagine im = new Immagine(input, l);
							multimedia[i] = im;
							// leggo la stringa vuota successiva non inserita
							// dall'utente; senza tale istruzione alla prossima
							// iterazione viene rilevata la stringa vuota
							input = kb.nextLine();
							System.out.println();
						} else {
							System.out.println(
									"E' richiesto un valore strettamente maggiore di 0.\n" + "Il programma termina.");
							System.exit(0);
						}
					} catch (InputMismatchException ex) {
						System.out.println(
								"ERRORE: è richieso un valore numerico intero positivo.\n" + "Il programma termina.");
						System.exit(0);
					}
				}
			}
		}

		// riproduzione/visualizzazione
		// il ciclo termina se l'utente inserisce "0"
		int scelta;
		try {
			do {
				System.out.println("Seleziona l'elemento multimediale da riprodurre o visualizzare:");
				for (int i = 0; i < multimedia.length; ++i) {
					System.out.println("premi (" + (i + 1) + ") per selezionare \"" + multimedia[i].getTitolo() + "\"");
				}
				scelta = kb.nextInt();
				if (scelta == 1) {
					((Audio) multimedia[0]).play();
					System.out.print("Vuoi modificare il volume?\n"
							+ "premi 1 per aumentare il volume, 2 per diminuirlo,\n3 per continuare la riproduzione: ");
					scelta = kb.nextInt();
					if (scelta == 1) {
						((Audio) multimedia[0]).louder();
					} else if (scelta == 2) {
						((Audio) multimedia[0]).weaker();
					} else {
						continue;
					}
				} else if (scelta == 2) {
					((Audio) multimedia[1]).play();
					System.out.print("Vuoi modificare il volume?\n"
							+ "premi 1 per aumentare il volume, 2 per diminuirlo,\n3 per continuare la riproduzione: ");
					scelta = kb.nextInt();
					if (scelta == 1) {
						((Audio) multimedia[1]).louder();
					} else if (scelta == 2) {
						((Audio) multimedia[1]).weaker();
					} else {
						continue;
					}
				} else if (scelta == 3) {
					((Filmato) multimedia[2]).play();
					System.out.print("Vuoi modificare il volume?\n"
							+ "premi 1 per aumentare il volume, 2 per diminuirlo,\n3 per continuare la riproduzione: ");
					scelta = kb.nextInt();
					if (scelta == 1) {
						((Filmato) multimedia[2]).louder();
					} else if (scelta == 2) {
						((Filmato) multimedia[2]).weaker();
					} else {
						continue;
					}
					System.out.print("Vuoi modificare anche la luminosita'?\n"
							+ "premi 1 per aumentarla, 2 per diminuirla,\n3 per continuare la riproduzione: ");
					scelta = kb.nextInt();
					if (scelta == 1) {
						((Filmato) multimedia[2]).brighter();
					} else if (scelta == 2) {
						((Filmato) multimedia[2]).darker();
					} else {
						continue;
					}
				} else if (scelta == 4) {
					((Immagine) multimedia[3]).show();
					System.out.print("Vuoi modificare anche la luminosita'?\n"
							+ "premi 1 per aumentarla, 2 per diminuirla,\n3 per continuare la riproduzione: ");
					scelta = kb.nextInt();
					if (scelta == 1) {
						((Immagine) multimedia[3]).brighter();
					} else if (scelta == 2) {
						((Immagine) multimedia[3]).darker();
					} else {
						continue;
					}
				} else if (scelta == 5) {
					((Immagine) multimedia[4]).show();
					System.out.print("Vuoi modificare anche la luminosita'?\n"
							+ "premi 1 per aumentarla, 2 per diminuirla,\n3 per continuare la riproduzione: ");
					scelta = kb.nextInt();
					if (scelta == 1) {
						((Immagine) multimedia[4]).brighter();
					} else if (scelta == 2) {
						((Immagine) multimedia[4]).darker();
					} else {
						continue;
					}
				} else if (scelta == 0) {
					System.out.println("Operazione terminata.");
					System.exit(0);
				} else {
					System.out.println("Scelta non valida, riprovare.");
				}
				System.out.println();
			} while (scelta != 0);

		} catch (InputMismatchException ex) {
			System.out.println("ERRORE: inserimento non consentito.\n" + "Il programma termina.");
			System.exit(0);
		}

	}
}
