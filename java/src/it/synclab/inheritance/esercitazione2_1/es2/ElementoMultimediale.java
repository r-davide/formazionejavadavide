/*
 * Un Elemento Multimediale è una Immagine, un Filmato o una registrazione Audio identificato 
 * da un titolo (una stringa non vuota). Un elemento è riproducibile se ha una durata (un valore 
 * positivo di tipo int) e un metodo play().
 */

package it.synclab.inheritance.esercitazione2_1.es2;

public class ElementoMultimediale {

	private String titolo;
	
	public ElementoMultimediale() {}

	public ElementoMultimediale(String titolo) {
		super();
		this.titolo = titolo;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

}
