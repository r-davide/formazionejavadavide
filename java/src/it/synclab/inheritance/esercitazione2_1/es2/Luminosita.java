package it.synclab.inheritance.esercitazione2_1.es2;

public interface Luminosita {
	public void brighter();
	public void darker();
}
