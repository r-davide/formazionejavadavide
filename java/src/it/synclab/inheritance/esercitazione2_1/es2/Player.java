package it.synclab.inheritance.esercitazione2_1.es2;

public interface Player {
	
	public void play();
	public void weaker();
	public void louder();
	
}
