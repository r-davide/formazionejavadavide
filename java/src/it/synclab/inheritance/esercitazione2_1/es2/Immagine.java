/*
 * Una Immagine non è riproducibile, ma ha una luminosita regolabile analoga a quella dei filmati e un metodo
 * show() che stampa il titolo concatenato a una sequenza di asterischi di lunghezza pari alla luminosita.
 */

package it.synclab.inheritance.esercitazione2_1.es2;

public class Immagine extends ElementoMultimediale implements Luminosita {
	private int luminosita;
	
	public Immagine() {
		super();
	}

	public Immagine(String titolo, int luminosita) {
		super(titolo);
		this.luminosita = luminosita;
	}

	public int getLuminosita() {
		return luminosita;
	}

	public void setLuminosita(int luminosita) {
		this.luminosita = luminosita;
	}

	public void show() {
		System.out.print(this.getTitolo() + " ");
		for (int i = 0; i < this.luminosita; i++) {
			System.out.print("*");
		}
		System.out.println();
	}

	@Override
	public void brighter() {
		this.luminosita += 1;
	}

	@Override
	public void darker() {
		this.luminosita -= 1;
	}

}
