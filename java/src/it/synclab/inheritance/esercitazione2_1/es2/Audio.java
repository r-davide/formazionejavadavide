/*
 * Una registrazione Audio è riproducibile e ha associato anche un volume (un valore positivo di tipo int)
 * e i metodi weaker() e louder() per regolarlo.
 * Se riprodotta, ripete per un numero di volte pari alla durata la stampa del titolo concatenato a una
 * sequenza di punti esclamativi di lunghezza pari al volume (una stampa per riga).
 */

package it.synclab.inheritance.esercitazione2_1.es2;

public class Audio extends ElementoMultimediale implements Player {
	private int durata;
	private int volume;
	
	public Audio() {
		super();
	}

	public Audio(String titolo, int durata, int volume) {
		super(titolo);
		this.durata = durata;
		this.volume = volume;
	}

	public int getDurata() {
		return durata;
	}

	public void setDurata(int durata) {
		this.durata = durata;
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	@Override
	public void play() {
		for (int i = 0; i < durata; i++) {
			System.out.print(this.getTitolo() + " ");
			for (int j = 0; j < volume; j++) {
				System.out.print("!");
			}
			System.out.println();
		}
	}

	public void weaker() {
		this.volume -= 1;
	}

	public void louder() {
		this.volume += 1;
	}

}
