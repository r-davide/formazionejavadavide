/*
 * Un Filmato è riproducibile e ha associato un volume regolabile analogo a quello delle 
 * registrazioni audio e anche una luminosita (un valore positivo di tipo int) e i metodi 
 * brighter() e darker() per regolarla. 
 * Se riprodotta, ripete per un numero di volte pari alla durata la stampa del titolo concatenato 
 * a una sequenza di punti esclamativi di lunghezza pari al volume e poi a una sequenza di 
 * asterischi di lunghezza pari alla luminosita (una stampa per riga).
 */

package it.synclab.inheritance.esercitazione2_1.es2;

public class Filmato extends ElementoMultimediale implements Player, Luminosita {
	private int durata;
	private Audio tracciaAudio;
	private int luminosita;

	public Filmato() {
		super();
	}
	
	public Filmato(String titolo, int durata, Audio tracciaAudio, int luminosita) {
		super(titolo);
		this.durata = durata;
		this.tracciaAudio = tracciaAudio;
		this.luminosita = luminosita;
	}

	public int getDurata() {
		return durata;
	}

	public void setDurata(int durata) {
		this.durata = durata;
	}

	public Audio getTracciaAudio() {
		return tracciaAudio;
	}

	public void setTracciaAudio(Audio tracciaAudio) {
		this.tracciaAudio = tracciaAudio;
	}

	public int getLuminosita() {
		return luminosita;
	}

	public void setLuminosita(int luminosita) {
		this.luminosita = luminosita;
	}

	@Override
	public void play() {
		for(int i = 0; i < durata; ++i) {
			System.out.print(this.getTitolo() + " ");
			for (int j = 0; j < this.tracciaAudio.getVolume(); ++j) {
				System.out.print("!");
			}
			System.out.print(" ");
			for (int k = 0; k < this.luminosita; ++k) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	@Override
	public void weaker() {
		this.tracciaAudio.weaker();
	}

	@Override
	public void louder() {
		this.tracciaAudio.louder();
	}

	@Override
	public void brighter() {
		this.luminosita += 1;
	}

	@Override
	public void darker() {
		this.luminosita -= 1;
	}

}
