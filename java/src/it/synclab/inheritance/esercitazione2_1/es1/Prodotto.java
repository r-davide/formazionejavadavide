/*
 * Il gestore di un negozio associa a tutti i suoi Prodotti un codice a barre 
 * univoco, una descrizione sintetica del prodotto e il suo prezzo unitario. 
 * Realizzare una classe Prodotti con le opportune variabili di istanza e metodi get.
 * Aggiungere alla classe Prodotti un metodo applicaSconto che modifica il prezzo 
 * del prodotto diminuendolo del 5%. 
 * Aggiungere alla classe Prodotti una implementazione specifica dei metodi 
 * ereditati dalla classe Object.
 */

package it.synclab.inheritance.esercitazione2_1.es1;

import java.util.regex.Pattern;

public class Prodotto {
	private String codice;
	private String descrizione;
	private double prezzo;

	public Prodotto(String codice, String descrizione, String prezzo) {
		setCodice(codice);
		setDescrizione(descrizione);
		setPrezzo(prezzo);
	}

	public void applicaSconto() {
		double prezzoScontato = this.prezzo - (this.prezzo * 0.05);
		prezzoScontato = Math.floor(prezzoScontato * 100) / 100;

		this.setPrezzo(Double.toString(prezzoScontato));
	}

	//metodo per stampare in console l'oggetto
	public void visualize() {
		System.out.println("Codice: " + codice + "\n" 
				+ "Descrizione: " + descrizione + "\n" 
				+ "Prezzo: euro " + prezzo + "\n");
	}

	//setta il codice a barre solo se questo è composto solo da cifre
	public void setCodice(String codice) {
		codice = codice.trim();
		if (codice == null) {
			System.out.println("Inserire un codice valido.");
			System.exit(0);
		} else if (Pattern.matches("[0-9]+", codice)) {
			this.codice = codice;
		} else {
			System.out.println("Inserire un codice valido.");
			System.exit(0);
		}
	}

	public void setDescrizione(String descrizione) {
		descrizione = descrizione.trim();
		if (descrizione == null) {
			System.out.println("Attenzione: descrizione dei prodotti obbligatoria.");
			System.exit(0);
		} else if (Pattern.matches("[A-Za-z0-9.,;\\-'\"\\s]+", descrizione)) {
			this.descrizione = descrizione;
		} else {
			System.out.println("Attenzione: descrizione dei prodotti obbligatoria.");
			System.exit(0);
		}
	}

	public void setPrezzo(String prezzo) {
		prezzo = prezzo.trim();
		if (prezzo == null || prezzo == "") {
			System.out.println("Inserire il prezzo del prodotto.\n" 
					+ "Utilizzare il carattere \".\" come separatore per i centesimi "
					+ "e per questi utilizzare 2 cifre.");
			System.exit(0);
		} else if (Pattern.matches("[0-9]+.[0-9]{2}", prezzo)) {
			this.prezzo = Double.parseDouble(prezzo);
		} else {
			System.out.println("Inserimento prezzo non valido.\n" 
					+ "Utilizzare il carattere \".\" come separatore per i centesimi "
					+ "e per questi utilizzare 2 cifre.");
			System.exit(0);
		}
	}

	public String getCodice() {
		return this.codice;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public double getPrezzo() {
		return this.prezzo;
	}
}
