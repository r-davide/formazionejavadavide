/*
 * Il gestore del negozio vuole fare una distinzione tra i prodotti Alimentari 
 * e quelli Non Alimentari. Ai prodotti Non Alimentari viene associato 
 * il materiale principale di cui sono fatti.
 * 
 * Realizzare la sottoclasse estendendo opportunamente la classe Prodotto.
 * 
 * Modificare la sottoclasse specializzando il metodo applicaSconto() in modo che
 * nel caso dei prodotti Non Alimentari venga applicato uno sconto del 10% se il 
 * prodotto è composto da un materiale riciclabile (carta, vetro o plastica).
 */

package it.synclab.inheritance.esercitazione2_1.es1;

import java.util.regex.Pattern;

public class ProdottoNonAlimentare extends Prodotto {
	private String materiale;

	public ProdottoNonAlimentare(String codice, String descrizione, String prezzo, String materiale) {
		super(codice, descrizione, prezzo);
		setMateriale(materiale);
	}
	
	//lo sconto del 10% viene applicato solo se il prodotto e' di plastica/carta/vetro
	public void applicaSconto() {
		double prezzoScontato;
		if (this.isRiciclabile()) {
			prezzoScontato = this.getPrezzo() - (this.getPrezzo() * 0.10);
			//tronca alla seconda cifra decimale
			prezzoScontato = Math.floor(prezzoScontato * 100) / 100; 

			this.setPrezzo(Double.toString(prezzoScontato));
		}
	}
	
	public void visualize() {
		System.out.println("Codice: " + this.getCodice() + "\n" 
				+ "Descrizione: " + this.getDescrizione()); 
		if (this.isRiciclabile()) {
			System.out.println("PREZZO SCONTATO: euro " + this.getPrezzo());
		} else {				
			System.out.println("Prezzo: euro " + this.getPrezzo());
		}
		System.out.println("Materiale: " + this.getMateriale() + "\n");

	}

	public void setMateriale(String materiale) {
		materiale = materiale.trim();
		if (materiale == null) {
			System.out.println("Inserire il nome del materiale del prodotto.\n"
					+ "Non è possibile inserire caratteri numerici.");
		} else if (Pattern.matches("[A-Za-z.,;\\-'\"\\s]+", materiale)) {
			this.materiale = materiale;
			this.applicaSconto();
		} else {
			System.out.println("Inserire il nome del materiale del prodotto."
					+ "Non è possibile inserire caratteri numerici.");

		}
	}

	public void setPrezzo(String prezzo) {
		super.setPrezzo(prezzo);
	}

	public String getMateriale() {
		return materiale;
	}

	//metodo di supporto per determinare se il prodotto è riciclabile
	public boolean isRiciclabile() {
		if (this.getMateriale().equalsIgnoreCase("plastica") 
				|| this.getMateriale().equalsIgnoreCase("vetro") 
				|| this.getMateriale().equalsIgnoreCase("carta")) {
			return true;
		} else {
			return false;
		}
	}
}
