/*
 * Realizzare una classe ListaSpesa che chieda all'utente di inserire i prodotti
 * acquistati e calcoli il totale della spesa applicando gli opportuni sconti se 
 * il cliente ha la tessera fedeltà.
 */

package it.synclab.inheritance.esercitazione2_1.es1;

import java.util.Scanner;

public class ListaSpesa {
	public static void main(String[] args) {

		ProdottoNonAlimentare p0 = new ProdottoNonAlimentare("80000", "Forbici", "4.99", "Ferro");
		ProdottoNonAlimentare p1 = new ProdottoNonAlimentare("80001", "Piatti piani", "2.99", "Plastica");
		ProdottoNonAlimentare p2 = new ProdottoNonAlimentare("80002", "Set coltelli", "19.99", "Acciaio, Plastica");
		ProdottoNonAlimentare p3 = new ProdottoNonAlimentare("80003", "Bicchiere", "4.49", "Vetro");
		ProdottoNonAlimentare p4 = new ProdottoNonAlimentare("80004", "Padella 28cm", "12.99", "Acciaio, Plastica");
		ProdottoAlimentare p5 = new ProdottoAlimentare("80005", "Salsa di pomodoro", "1.99", "31/10/2021");
		ProdottoAlimentare p6 = new ProdottoAlimentare("80006", "Patate al forno", "2.99", "27/12/2019");
		ProdottoAlimentare p7 = new ProdottoAlimentare("80007", "Baguette", "1.19", "30/04/2020");
		ProdottoAlimentare p8 = new ProdottoAlimentare("80008", "Spaghetti n.5", "1.19", "18/04/2022");
		ProdottoAlimentare p9 = new ProdottoAlimentare("80009", "Grana Padano", "4.99", "31/03/2020");
		Prodotto[] listaProdotti = { p0, p1, p2, p3, p4, p5, p6, p7, p8, p9 };

		ListaSpesa.iniziaSpesa(listaProdotti);
	}

	public static void iniziaSpesa(Prodotto[] listaProdotti) {
		Scanner kb = new Scanner(System.in);
		String input;
		System.out.print("Benvenuto! \nVuoi iniziare la spesa? (y / n): ");
		do {
			input = kb.next();
			if (!(input.equalsIgnoreCase("y")) && !(input.equalsIgnoreCase("n"))) {
				System.out.println("Inserimento non valido. \n"
						+ "Inserire \"y\" per iniziare la spesa, \"n\" per terminare.\n" + "Qual e' la tua scelta?");
			}
		} while (!(input.equalsIgnoreCase("y")) && !(input.equalsIgnoreCase("n")));

		if (input.equalsIgnoreCase("y")) {
			System.out.println("Iniziamo! Segui le istruzioni:" + "Inserisci un prodotto alla volta.\n");
		} else {
			System.out.println("Arrivederci!");
			System.exit(0);
		}
		System.out.println("Premi:");
		for (int i = 0; i < listaProdotti.length; i++) {
			System.out.println(i + " per acquistare " + listaProdotti[i].getDescrizione());
		}
		System.out.println("Infine, premi \"t\" per terminare la spesa.");

		// l'utente sceglie i prodotti e questi vengono inseriti in una stringa
		boolean termina = false;
		String scelteUtente = "";
		do {
			input = kb.next();
			termina = input.equalsIgnoreCase("t");
			if (!input.matches("[0-9]") && !input.equalsIgnoreCase("t")) {
				System.out.println("Scelta non valida. Inserire un carattere tra quelli indicati.");
			} else if (input.equalsIgnoreCase("t")) {
				System.out.println("Terminato...\n");
			} else {
				// coverte l'input in codice a barre
				int cod = Integer.parseInt(input) + 80000;
				// aggiunge i codici in una stringa
				scelteUtente += cod + ",";
				System.out.println("Aggiunto al carrello!");
			}
		} while (!termina);

		// inserisco le scelte in un array
		String[] acquisto = scelteUtente.split(",");
		if (!scelteUtente.equals("")) {
			// se la stringa non è vuota calcola il totale
			calcolaTotale(listaProdotti, acquisto);
		}
	}

	/*
	 * trova i prodotti scelti dall'utente in listaProdotti, stampa a video il
	 * prodotto con il prezzo scontato se previsto, e calcola il totale
	 */
	private static void calcolaTotale(Prodotto[] listaProdotti, String[] codice) {
		double totale = 0;
		System.out.println("Prodotti acquistati: ");
		for (String cod : codice) {
			for (int i = 0; i < listaProdotti.length; i++) {
				if (cod.equalsIgnoreCase(listaProdotti[i].getCodice())) {
					totale += listaProdotti[i].getPrezzo();
					listaProdotti[i].visualize();
				}
			}
		}
		// tronco il numero alla seconda cifra decimale
		totale = Math.ceil(totale * 100) / 100;
		System.out.println("Totale spesa:  euro " + totale);
	}
}
