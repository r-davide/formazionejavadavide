/*
 * Il gestore del negozio vuole fare una distinzione tra i prodotti Alimentari 
 * e quelli Non Alimentari. Ai prodotti Alimentari viene associata una 
 * data di scadenza (si veda la classe Data).
 * 
 * Realizzare la sottoclasse estendendo opportunamente la classe Prodotto.
 * 
 * Modificare la sottoclasse specializzando il metodo applicaSconto() in modo che 
 * nel caso dei prodotti Alimentari venga applicato uno sconto del 20% se la data 
 * di scadenza è a meno di 10 giorni dalla data attuale (si usi il metodo 
 * getDifference() della classe Data).
 */

package it.synclab.inheritance.esercitazione2_1.es1;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;

public class ProdottoAlimentare extends Prodotto {
	private LocalDate dataScadenza;

	public ProdottoAlimentare(String codice, String descrizione, String prezzo, String dataScadenza) {
		super(codice, descrizione, prezzo);
		setDataScadenza(dataScadenza);
	}

	//lo sconto del 20% viene applicato solo se mancano meno di 10 giorni dalla scadenza
	public void applicaSconto() {
		double prezzoScontato = this.getPrezzo() - (this.getPrezzo() * 0.20);
		prezzoScontato = Math.floor(prezzoScontato * 100) / 100;
		if (this.getDifference() < 10) {
			this.setPrezzo(Double.toString(prezzoScontato));
		}
	}
	
	//metodo per stampare in console l'oggetto
	public void visualize() {
		try {
			//imposto il formato italiano della data 
			DateTimeFormatter formatoData = DateTimeFormatter.ofPattern("dd/MM/yyyy");
			String data = this.dataScadenza.format(formatoData);
			System.out.println("Codice: " + this.getCodice() + "\n" 
					+ "Descrizione: " + this.getDescrizione()); 
			if (this.getDifference() < 10) {
				System.out.println("PREZZO SCONTATO: euro " + this.getPrezzo());
			} else {				
				System.out.println("Prezzo: euro " + this.getPrezzo());
			}
			System.out.println("Scade il: " + data + "\n");
		} catch (Exception e) {
			System.out.println();
		}
	}

	/*
	 * se la stringa che contiene la data di scadenza corrisponde dalla regex 
	 * viene inserita in una LocalDate temporanea per verificare che tale data inserita
	 * non sia antecedente la data di oggi, quindi che il prodotto non sia scaduto
	 */
	public void setDataScadenza(String dataScadenza) {
		dataScadenza = dataScadenza.trim();
		String regexData = "^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/[1-9][0-9][0-9][0-9]$";
		if (dataScadenza == null) {
			System.out.println("Data non valida. \nInserire nel formato GG/MM/YYYY.");
			System.exit(0);
		} else if (Pattern.matches(regexData, dataScadenza)) {
			//salvo giorno, mese e anno dentro un array
			String[] data = dataScadenza.split("/");
			//creo una LocalDate
			LocalDate tmp = LocalDate.of(Integer.parseInt(data[2]), 
					Integer.parseInt(data[1]), Integer.parseInt(data[0]));
			if (ChronoUnit.DAYS.between(LocalDate.now(), tmp) > 0) {
				this.dataScadenza = tmp;
			} else {
				System.out.println("Inserimento negato, prodotto già scaduto.");
				System.exit(0);
			}
			this.applicaSconto();
		} else {
			System.out.println("Data non valida. \nInserire nel formato GG/MM/YYYY.");
			System.exit(0);
		}
	}
	
	/*
	 * settare applicaSconto all'interno di setPrezzo. 
	 * Se si verificano le condizioni allora applica
	 */
	public void setPrezzo(String prezzo) {
		super.setPrezzo(prezzo);
//		this.applicaSconto();
	}

	public LocalDate getDataScadenza() {
		return dataScadenza;
	}
	
	// metodo di supporto per determinare la differenza tra la data attuale e
	// e la data di scadenza del prodotto
	private long getDifference() {
		long diff = ChronoUnit.DAYS.between(LocalDate.now(), this.dataScadenza);
		return diff;
	}
	
}