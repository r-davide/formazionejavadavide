/*
 * Scrivere la classe MultipleQuestion che estende la classe NumericQuestion in modo 
 * da rappresentare domande che offrono un certo numero di opzioni (prefissato) e 
 * alle quali possa essere risposto solo con un valore intero positivo minore o 
 * uguale al numero di opzioni disponibili.
 * Sovrascrivere il metodo ask() in modo da garantire che l’utente risponda con un 
 * valore consentito (prima che venga restituito il punteggio conseguito).
 */

package it.synclab.inheritance.esercitazione2.es5;

public class MultipleQuestion extends NumericQuestion {
	private String[] options;

	public MultipleQuestion(String question, String answer, int score) {
		super(question, answer, score);
	}


	public void setAnswer(String answer) {
		splitAnswer(answer);
		for (String opt : options) {
			if (isNumString(opt)) {

			} 
	}

	public String[] getOptions() {
		return this.options;
	}

	private void splitAnswer(String answer) {
		options = answer.split(" ");
	}

	private void printOptions() {
		for (String str : this.getOptions()) {
			System.out.print(str + " ");
		}
	}

	public static void main(String[] args) {
		MultipleQuestion m = new MultipleQuestion("Question?", "32 34 34 34", 3);
		System.out.println(m.getQuestion());
		System.out.println(m.getAnswer());
		System.out.println();
		m.printOptions();

	}

}
