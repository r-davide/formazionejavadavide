/*
 * Scrivere la classe Question i cui oggetti rappresentano domande di esami orali. 
 * Ogni quesito si compone di una domanda, di una risposta corretta e di un 
 * punteggio, e mette a disposizione un metodo ask() che stampa la domanda, legge 
 * la risposta dell’utente e restituisce il punteggio conseguito (0 se la risposta 
 * dell’utente `e sbagliata).
 */

package it.synclab.inheritance.esercitazione2.es5;

import java.util.regex.Pattern;
import java.util.Scanner;

public class Question {
	private String question, answer;
	private int score;

	public Question(String question, String answer, int score) {
		this.setQuestion(question);
		this.setAnswer(answer);
		this.setScore(score);
	}

	public int ask() {
		Scanner kb = new Scanner(System.in);
		if (isValidQue(this.question) && isValidAns(this.answer)) {
			System.out.println("Domanda: " + question);

			System.out.println("Risposta dell'utente: ");
			String input = kb.nextLine();

			if (input.equals(answer)) {
				System.out.println("Risposta corretta. Punteggio: " + score);
			} else {
				score = 0;
				System.out.println("Risposta sbagliata. Punteggio: " + score);
			}
		}

		return score;
	}

	public void setQuestion(String question) {
		question = question.trim();
		if (isValidQue(question)) {
			// rimuove eventuali spazi iniziali o finali se presenti
			this.question = question;
		} else {
			System.out.println("Inserire una domanda valida.\n" + "Non può iniziare con un numero,\n"
					+ "non può essere vuota,\n" + "non può essere solo un numero,\n"
					+ "deve terminare con il carattere \"?\"" + "oppure con un punto");
		}
	}

	public void setAnswer(String answer) {
		answer = answer.trim();
		if (isValidAns(answer)) {
			this.answer = answer;
		} else {
			System.out.println("Inserire una risposta valida.");
		}
	}

	// inserire blocco try-catch se inserisco un carattere
	public void setScore(int score) {
		if (score >= 0) {
			this.score = score;
		} else {
			System.out.println("Inserire un valore intero maggiore di zero.");
		}
	}

	public String getQuestion() {
		return question;
	}

	public String getAnswer() {
		return answer;
	}

	public int getScore() {
		return score;
	}

	// validazione della domanda in input mediante regex
	protected boolean isValidQue(String str) {
		boolean isValid = false;
		if (str == null) return isValid;
		// non ammette numeri a inizio stringa
		// deve terminare con un punto di domanda o con un punto
		if (Pattern.matches("[^0-9][A-Za-z0-9=.?\",;'()\\-\\+\\*/\\s]+[.?]$", str)) {
			isValid = true;
		}
		return isValid;
	}
	//validazione della risposta in input mediante regex
	protected boolean isValidAns(String str) {
		boolean isValid = false;
		if (str == null) return isValid;
		if (Pattern.matches("[A-Za-z0-9,;\\-'\"\\s]+", str)) {
			isValid = true;
		} else {
			isValid = false;
		}
		return isValid;
	}
	
}
