/*
 * Scrivere la classe NumericQuestion che estende la classe Question in modo da 
 * rappresentare domande a cui possa essere risposto solo con un valore intero. 
 * Sovrascrivere il metodo ask() in modo da garantire che l’utente risponda con 
 * un valore intero (prima che venga restituito il punteggio conseguito).
 */

package it.synclab.inheritance.esercitazione2.es5;

import java.util.Scanner;

public class NumericQuestion extends Question {	
	public NumericQuestion(String question, String answer, int score) {
		super(question, answer, score);
	}

	public int ask() {
		Scanner kb = new Scanner(System.in);
		if (this.isValidQue(this.getQuestion()) && this.isValidAns(this.getAnswer())) {
			System.out.println("Domanda: " + this.getQuestion());
			System.out.println("Rispondere indicando un valore intero (anche negativo): ");
			String input = kb.nextLine();

			if (isNumString(input)) {
				if (input.equals(this.getAnswer())) {
					System.out.println("Risposta corretta. Punteggio: " + this.getScore());
				} else {
					this.setScore(0);
					System.out.println("Risposta sbagliata. Punteggio: " + this.getScore());
				}
			} else {
				System.out.println("E' possibile rispondere solo indicando un valore intero");
			}
		}
		return this.getScore();
	}

	public void setAnswer(String answer) {
		answer = answer.trim();
		if (this.isNumString(answer)) {
			super.setAnswer(answer);
		} else {
			System.out.println("La risposta che si vuole inserire non e' valida.\n" 
					+ "E' possibile rispondere solo indicando un valore intero.");
		}
	}

	protected boolean isNumString(String str) {
		str = str.trim();
		boolean isNum = true;
		int i = 0;
		if (str.charAt(0) == '-') i++;
		while (i < str.length() && isNum) {
			if (Character.isDigit(str.charAt(i))) {
				isNum = true;
				i++;
			} else {
				isNum = false;
			}
		}
		return isNum;
	}

}
