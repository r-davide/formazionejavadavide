/*
 * Scrivere la classe QuestionYesNo che estende la classe Question in modo da 
 * rappresentare domande a cui possa essere risposto solo si o no. 
 * Sovrascrivere il metodo ask() in modo da garantire che l’utente risponda si 
 * o no (prima che venga restituito il punteggio conseguito).
 * */

package it.synclab.inheritance.esercitazione2.es5;

import java.util.Scanner;

public class QuestionYesNo extends Question {

	public QuestionYesNo(String question, String answer, int score) {
		super(question, answer, score);
	}

	public int ask() {
		Scanner kb = new Scanner(System.in);
		if (isValidQue(this.getQuestion()) && isValidAns(this.getAnswer())) {
			System.out.println("Domanda: " + this.getQuestion());
			System.out.println("Rispondere indicando \"si\" oppure \"no\": ");
			String input = kb.nextLine();

			if (input.equals("si") || input.equals("no")) {
				if (input.equals(this.getAnswer())) {
					System.out.println("Risposta corretta. Punteggio: " + this.getScore());
				} else {
					this.setScore(0);
					System.out.println("Risposta sbagliata. Punteggio: " + this.getScore());
				}
			} else {
				System.out.println("Risposta non valida.\n"
						+ "E' possibile rispondere indicando \"si\" oppure \"no\"");
			}
		}

		return this.getScore();
	}

	public void setAnswer(String answer) {
		answer = answer.trim();
		if (answer.equals("si") || answer.equals("no")) {
			super.setAnswer(answer);
		} else {
			System.out.println("La risposta che si vuole inserire non e' valida.\n" 
					+ "E' possibile rispondere \"si\" oppure \"no\"");
		}
	}


}
