/*
 * Scrivere la classe Motocycle che estende la classe Vehicle. 
 * La classe Motocycle prevede un numero che ne descrive la cilindrata (50, 125, ....).
 */

package it.synclab.inheritance.esercitazione2.es3;

public class Moto extends Veicolo {
	private int cilindrata;

	public Moto() {
	}

	public Moto(String marca, String modello, String targa, int cilindrata) {
		super(marca, modello, targa);
		this.cilindrata = cilindrata;
	}

	public void setCilindrata(int cilindrata) {
		if (cilindrata > 0) {
			this.cilindrata = cilindrata;
		} else {
			System.out.println("Input non valido.");
		}
	}

	public int getCilindrata() {
		return cilindrata;
	}
}
