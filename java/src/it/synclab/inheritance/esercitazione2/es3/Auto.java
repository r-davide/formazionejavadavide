/*
 * Scrivere la classe Car che estende la classe Vehicle. 
 * La classe Car prevede una stringa che ne descrive la tipologia ("utilitaria",
 * "station wagon", "SUV",....).
 */

package it.synclab.inheritance.esercitazione2.es3;

public class Auto extends Veicolo {
	private String tipologia;

	public Auto() {}

	public Auto(String marca, String modello, String targa, String tipologia) {
		super(marca, modello, targa);
		this.tipologia = tipologia;
	}

	public void setTipologia(String tipologia) {
		if (tipologia != null || tipologia != "") {
			this.tipologia = tipologia;
		}
	}

	public String getTipologia() {
		return tipologia;
	}
}
