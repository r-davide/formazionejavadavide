/*
 * Per testare le classi, scrivere un programma TestVehicles che crea un array 
 * inizializzato con veicoli delle varie tipologie. Alcuni dei veicoli inseriti 
 * nell’array dovranno diventare guasti. Il programma deve stampare la lista delle 
 * targhe dei veicoli guasti.
 */

package it.synclab.inheritance.esercitazione2.es3;

public class TestVeicoli {

	public static void main(String[] args) {
		Veicolo[] v = new Veicolo[3];
		v[0] = new Auto("Porsche", "Cayenne", "FJ342SS", "SUV");
		v[1] = new Auto("Fiat", "Panda", "DE230WS", "utilitaria");
		v[2] = new Moto("Honda", "CBR", "DA42124", 1000);

		for (int i = 0; i < v.length; i++) {
			if (v[i].getMarca() == null || v[i].getMarca() == "" || v[i].getModello() == null
					|| v[i].getModello() == "") {
				System.out.println("Attenzione: stringa vuota.");
				System.exit(0);
			}
		}

		v[0].isGuasto(true);
		v[2].isGuasto(true);

		System.out.println("Tutti i veicoli:");
		for (Veicolo veicolo : v) {
			veicolo.print();
		}

		System.out.println("\nVeicoli guasti:");
		for (Veicolo veicolo : v) {
			if (veicolo.isGuasto()) {
				veicolo.print();

			}
		}
	}
}
