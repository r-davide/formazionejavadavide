/*
 * Scrivere una classe Vehicle (veicolo) che prevede una targa, una marca e un modello.
 * La classe prevede anche una variabile booleana che descrive se il veicolo è guasto.
 * Aggiungere un costruttore opportuno e vari metodi get e set opportuni.
 */

package it.synclab.inheritance.esercitazione2.es3;

import java.util.regex.Pattern;

public class Veicolo {

	private String marca, modello, targa;
	private boolean guasto;

	public Veicolo() {
	}

	public Veicolo(String marca, String modello, String targa) {
		this.modello = modello;
		this.marca = marca;
		if (targaValida(targa)) {
			this.targa = targa;
		} else {
			System.out.println("Attenzione: targa non valida.");
			System.exit(0);
		}
	}

	public void isGuasto(boolean guasto) {
		this.guasto = guasto;
	}

	public boolean isGuasto() {
		return guasto;
	}

	public void setTarga(String targa) {
		if (!targaValida(targa)) {
			System.out.println("La targa " + targa + " non è valida.");
			System.exit(0);
		} else {
			this.targa = targa;
		}
	}

	public void setMarca(String marca) {
		if (marca != null || marca != "") {
			this.marca = marca;
		}
	}

	public void setModello(String modello) {
		if (modello != null || modello != "") {
			this.modello = modello;
		}
	}

	public String getTarga() {
		return targa;
	}

	public String getMarca() {
		return marca;
	}

	public String getModello() {
		return modello;
	}

	public void print() {
		System.out.println(this.getTarga() + " " + this.getMarca() + " " + this.getModello());
	}

	private boolean targaValida(String targa) {
		targa = targa.toUpperCase();
		String regexAuto = "[A-Z]{2}[0-9]{3}[A-Z]{2}";
		String regexMoto = "[A-Z]{2}[0-9]{5}";
		if (this instanceof Auto && Pattern.matches(regexAuto, targa)) {
			return true;
		} else if (this instanceof Moto && Pattern.matches(regexMoto, targa)) {
			return true;
		} else {
			return false;
		}
	}
}
