/*
 * Scrivere una classe Employee (dipendente) che estende la classe Person.
 * 
 * Ogni dipendente ha un proprio anno di assunzione e un proprio stipendio. 
 * Si definiscano costruttori e vari metodi get e set opportuni. 
 * Si ridefinisca inoltre il metodo visualize() opportunamente. 
 * Si definisca inoltre un metodo gainsMore che prende come parametro un 
 * altro oggetto Employee e restituisce true se l’oggetto corrente ha uno 
 * stipendio maggiore di quello ricevuto come parametro, o false altrimenti.
 */

package it.synclab.inheritance.esercitazione2.es2;

import it.synclab.inheritance.esercitazione2.es1.Person;

public class Employee extends Person {

	private double stipendio;
	private int annoAss;

	public Employee() {
		super();
	}

	public Employee(String name, String surname, String taxCode, String city, double stipendio, int annoAss) {
		super(name, surname, taxCode, city);
		this.stipendio = stipendio;
		this.annoAss = annoAss;
	}
	
	public void visualize() {
		System.out.println(this.getName() + ", " + this.getSurname() + ", " + this.stipendio);
	}
	
	public boolean gainsMore(Employee e) {
		if (this.stipendio > e.stipendio) {
			return true;
		} else {
			return false;
		}
	}

	public void setStipendio(double stipendio) {
		if (stipendio > 0) {
			this.stipendio = stipendio;
		} else {
			System.out.println("Valore non valido.");
		}
	}
	
	public void setAnnoAss(int annoAss) {
		if (annoAss > 0) {
			this.annoAss = annoAss;
		} else {
			System.out.println("Valore non valido");
		}
	}
	
	public double getStipendio() {
		return stipendio;
	}

	public int getAnnoAss() {
		return annoAss;
	}
	
}
