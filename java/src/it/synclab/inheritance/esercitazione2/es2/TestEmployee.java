/*
 * Per testare la classe, scrivere un programma TestEmployee che crea tre oggetti
 * della classe Employee e li visualizza in ordine di stipendio (usando il nuovo
 * metodo per confrontare gli stipendi).
 */

package it.synclab.inheritance.esercitazione2.es2;

import java.util.regex.Pattern;

public class TestEmployee {
	
	public static void sortByStipendio(Employee[] e) {
		for (int i = 1; i < e.length; i++) {
			Employee current = e[i];
			int prev = i - 1;
			while (prev >= 0 && e[prev].gainsMore(current)) {
				e[prev + 1] = e[prev];
				prev--;
			}
			e[prev + 1] = current;
		}
	}

	public static void main(String[] args) {
		Employee e1 = new Employee("Paolo", "Rossi", "PLARSS80H02F205W", "Milano", 1200.00, 2001);
		Employee e2 = new Employee("Mario", "Bianchi", "MRABCH90C09A944A", "Bologna", 1290.00, 2003);
		Employee e3 = new Employee("Bruno", "Esposito", "BRNSST91C03F839M", "Napoli", 920.00, 2015);

		Employee[] e = {e1, e2, e3};
		for(int i = 0; i < e.length; ++i) {
			if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]", e[i].getName())) {
				System.out.println("Attenzione stringa inserita per il nome non valida.\nRiprovare.");
				System.exit(0);
			} 
			if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]", e[i].getSurname())) {
				System.out.println("Attenzione stringa inserita per il cognome non valida.\nRiprovare.");
				System.exit(0);
			} 
			if (!Pattern.matches("[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]", e[i].getTaxCode())) {
				System.out.println("Codice fiscale non valido.");
				System.exit(0);
			}
			if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]", e[i].getCity())) {
				System.out.println("Attenzione stringa inserita per la citta' non valida.\nRiprovare.");
				System.exit(0);
			}
		}
	 
		sortByStipendio(e);
	
		for (int i = 0; i < e.length; i++) {
			e[i].visualize();
		}
	
	}
}
