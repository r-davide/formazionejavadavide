/*
 * Per testare le classi, scrivere un programma TestGarage che crea un certo numero di 
 * veicoli e un oggetto di tipo Garage, e usa il metodo repair() varie volte su oggetti 
 * diversi (guasti o meno) stampando i prezzi ottenuti.
 */

package it.synclab.inheritance.esercitazione2.es4;

import it.synclab.inheritance.esercitazione2.es3.*;

public class TestGarage {

	public static void main(String[] args) {
		Auto panda = new Auto("Fiat", "Panda", "DE230WS", "utilitaria");
		Auto cayenne = new Auto("Porsche", "Cayenne", "FJ342SS", "SUV");
		Moto cbr = new Moto("Honda", "CBR", "DA42155", 1000);
		Moto ciao = new Moto("Piaggio", "Ciao", "CH35813", 50);
		Moto tmax = new Moto("Yamaha", "TMax", "AK47155", 500);

		// inserisco veicoli in un array per stampare
		Veicolo[] v = { panda, cayenne, cbr, ciao, tmax };
		
		for (int i = 0; i < v.length; i++) {
			if (v[i].getMarca() == null || v[i].getMarca() == "" || v[i].getModello() == null
					|| v[i].getModello() == "") {
				System.out.println("Attenzione: stringa vuota.");
				System.exit(0);
			}
		}
		
		System.out.println("Veicoli creati: ");
		for (Veicolo veicolo : v) {
			veicolo.print();
		}

		// panda e ciao sono guasti
		panda.isGuasto(true);
		ciao.isGuasto(true);
		System.out.println("\nVeicoli guasti:");
		for (Veicolo veicolo : v) {
			if (veicolo.isGuasto()) {
				veicolo.print();

			}
		}

		Garage garage = new Garage();

		System.out.println("\nI veicoli entrano in garage... \n");
		for (Veicolo veicolo : v) {
			veicolo.print();
			double costo = garage.repair(veicolo);
			System.out.println("Prezzo garage: " + costo + "\n");
		}
	}
}
