/*
 * Scrivere una classe Garage che prevede solo un metodo repair() che utilizza 
 * veicoli (come definiti nell’esercizio precedente). Tale metodo prende un veicolo 
 * come parametro, ne cambia (se necessario) il valore della variabile booleana 
 * che descrive lo stato di guasto e restituisce come risultato il prezzo dell’intervento.
 * Il prezzo deve variare a seconda che il veicolo fosse guasto o meno, e a seconda 
 * della tipologia di veicolo.
 */

package it.synclab.inheritance.esercitazione2.es4;

import it.synclab.inheritance.esercitazione2.es3.*;

public class Garage {
	private final double AUTO_PRICE = 21.00;
	private final double MOTO_PRICE = 13.00; 
	private final double SURCHARGE = 0.50;
	
	public Garage() {}

	public double repair(Veicolo v) {
		double total = 0;

		if (v instanceof Auto) {
			if (v.isGuasto()) {
				total = AUTO_PRICE + AUTO_PRICE * SURCHARGE;
			} else {
				total = AUTO_PRICE;
			}
		}
		if (v instanceof Moto) {
			if (v.isGuasto()) {
				total = MOTO_PRICE + MOTO_PRICE * SURCHARGE;
			} else {
				total = MOTO_PRICE;
			}
		}

		return total;
	}
}
