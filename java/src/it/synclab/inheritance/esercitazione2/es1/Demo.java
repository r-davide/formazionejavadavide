/*
 * Creare un&#39;applicazione Java che istanzi un oggetto della classe Person
 * e ne visualizzi in seguito nome, cognome ed anno di nascita.
 * 
 * Creare tre oggetti di tipo Stagista memorizzarli in un array e 
 * visualizzare lo Stagista più giovane.
 */

package it.synclab.inheritance.esercitazione2.es1;
import java.util.regex.Pattern;

public class Demo {
	
	public static void main(String[] args) {
		Person p = new Person("Mario", "Rossi", "MRARSS80A01F205W", "Milano");
		if (!Pattern.matches("[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]", p.getTaxCode())) {
			System.out.println("Codice fiscale non valido.");
			System.exit(0);
		}
		if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]*", p.getName())) {
			System.out.println("Attenzione stringa inserita per il nome non valida.\nRiprovare.");
			System.exit(0);
		} 
		if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]*", p.getSurname())) {
			System.out.println("Attenzione stringa inserita per il cognome non valida.\nRiprovare.");
			System.exit(0);
		} 
		if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]", p.getCity())) {
			System.out.println("Attenzione stringa inserita per la citta' non valida.\nRiprovare.");
			System.exit(0);
		}
		
		p.visualize();
		System.out.println();
		
		Stagista s1 = new Stagista("Paolo", "Rossi", "PLARSS80H02F205W", "Milano", 2, 1);
		Stagista s2 = new Stagista("Mario", "Bianchi", "MRABCH90C09A944A", "Bologna", 4, 2);
		Stagista s3 = new Stagista("Bruno", "Esposito", "BRNSST91C03F839M", "Napoli", 8, 3);
		
		Stagista[] stag = {s1, s2, s3};
		for(int i = 0; i < stag.length; ++i) {
			if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]", stag[i].getName())) {
				System.out.println("Attenzione stringa inserita per il nome non valida.\nRiprovare.");
				System.exit(0);
			} 
			if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]", stag[i].getSurname())) {
				System.out.println("Attenzione stringa inserita per il cognome non valida.\nRiprovare.");
				System.exit(0);
			} 
			if (!Pattern.matches("[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]", stag[i].getTaxCode())) {
				System.out.println("Codice fiscale non valido.");
				System.exit(0);
			}
			if (!Pattern.matches("[a-zA-Z]+\\s?[a-zA-Z]", stag[i].getCity())) {
				System.out.println("Attenzione stringa inserita per la citta' non valida.\nRiprovare.");
				System.exit(0);
			}
		}
		
		Person younger = new Person();
		younger.setTaxCode(stag[0].getTaxCode());
		for (int i = 0; i < stag.length; i++) {
			if (stag[i].bornYear() > younger.bornYear()) {
				younger = stag[i];
			}
		}
		System.out.println("Lo stagista piu giovane e': ");
		younger.visualize();
	}
}
