/*
 * Creare una classe di nome Person con le variabili di istanza: 
 * surname, name, tax code e city di tipo stringa.
 * La classe deve contenere un costruttore di default che inizializzi 
 * le variabili di istanza con NULL; un costruttore parametrico; i metodi 
 * set e get ed un metodo chiamato bornYear che a partire dal codice fiscale
 * ricavi e restituisca l&#39;anno di nascita di una persona.
*/

package it.synclab.inheritance.esercitazione2.es1;

public class Person {

	private String name, surname, taxCode, city;

	public Person() {
	}

	public Person(String name, String surname, String taxCode, String city) {
		this.name = name;
		this.surname = surname;
		this.taxCode = taxCode;
		this.city = city;
	}

	public int bornYear() {
		int year;
		String tc = this.getTaxCode();
		year = Integer.parseInt(tc.substring(6, 8));

		return year;
	}
	
	public void visualize() {
		System.out.println(this.name + ", " + this.surname + ", " + this.bornYear());
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public String getCity() {
		return city;
	}

}
