/*
 * Costruire una sottoclasse di Person, chiamata Stagista, che contiene 2 
 * variabili di istanza entrambe di tipo intero:
 * - numberOfPresence, che registra il numero di ore di presenza
 * - idNumber (numero identificativo).
 * La sottoclasse deve contenere un costruttore parametrico ed i metodi set e get.
 */

package it.synclab.inheritance.esercitazione2.es1;

public class Stagista extends Person {
	private int numberOfPresence, idNumber;

	public Stagista(String name, String surname, String taxCode, String city, int numberOfPresence, int idNumber) {
		super(name, surname, taxCode, city);
		this.numberOfPresence = numberOfPresence;
		this.idNumber = idNumber;
	}

	public void setNumberOfPresence(int numberOfPresence) {
		if (numberOfPresence >= 0) {
			this.numberOfPresence = numberOfPresence;
		} else {
			System.out.println("The number of presence can't be negative.");
		}
	}

	public int getNumberOfPresence() {
		return numberOfPresence;
	}

	public int getID() {
		return idNumber;
	}

}
