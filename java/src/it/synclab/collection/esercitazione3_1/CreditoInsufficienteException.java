package it.synclab.collection.esercitazione3_1;

public class CreditoInsufficienteException extends Exception {

	public CreditoInsufficienteException() {
		super();
	}

	public CreditoInsufficienteException(String message) {
		super(message);
	}

}
