/*
 * Il distributore di bevande è in grado di erogare alcuni tipi di bevande;
 * ogni bevanda è caratterizzata da un codice, nome ed un prezzo.
 * 
 * Il metodo aggiungiBevanda() permette di aggiungere la descrizione di una
 * bevanda. Il distributore permette di conoscere, dato il codice, il prezzo
 * e il nome della bevanda tramite i metodi getPrice() e getName()
 * rispettivamente. Quando si richiede un codice corrispondente ad una
 * bevanda non disponibile, viene generata un’eccezione di BevandaNonValida.
 */

package it.synclab.collection.esercitazione3_1;

public class Bevanda {
	private String codice;
	private String nome;
	private double prezzo;
	
	public Bevanda() {}

	public Bevanda(String codice, String nome, double prezzo) {
		super();
		this.codice = codice;
		this.nome = nome;
		this.prezzo = prezzo;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
	
	public String toString() {
		return "[codice = " + codice + ", bevanda = " + nome + ", prezzo = " + prezzo + "]";
	}
}
