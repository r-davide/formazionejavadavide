package it.synclab.collection.esercitazione3_1;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class TestClass {

	private static Scanner kb;

	public static void main(String[] args)
			throws BevandaNonValidaException, TesseraNonValidaException, CreditoInsufficienteException {
		kb = new Scanner(System.in);

		// crea il contenitore delle bevande, ossia delle varie colonne
		LinkedList<LinkedList<Bevanda>> colonne = new LinkedList<>();
		// crea istanza distributore
		Distributore distributore = new Distributore(colonne);

		// creazione bevande
		LinkedList<Bevanda> bevanda = new LinkedList<>();
		String bin;
		System.out.println("Inserimento bevande...");
		do {
			System.out.print("\nInserisci l'ID della bevanda: ");
			bin = kb.next();
			Bevanda b = new Bevanda();
			b.setCodice(bin);

			bin = kb.nextLine(); // per eliminare la riga vuota che altrimenti memorizza in setNome

			System.out.print("Ora inserisci il nome della bevanda: ");
			bin = kb.nextLine();
			b.setNome(bin);

			boolean match = false;
			do {
				System.out.println(
						"Inserisci il prezzo \n(usare come separatore il punto e inserire due cifre decimali): ");
				bin = kb.next();
				if (Pattern.matches("[0-9]+[.]?[0-9]{2}", bin)) {
					b.setPrezzo(Double.parseDouble(bin));
					match = true;
				} else {
					System.out.println("Inserimento non corretto, riprovare!\n");
					match = false;
				}
			} while (!match);

			bevanda.add(b);

			while (!bin.equalsIgnoreCase("n") && !bin.equalsIgnoreCase("y")) {
				System.out.print("\nVuoi aggiungere un'altro tipo bevanda? [y/n] ");
				bin = kb.next();
				if (!bin.equalsIgnoreCase("n") && !bin.equalsIgnoreCase("y")) {
					System.out.print("Inserisci \"y\" per continuare oppure \"n\" per terminare l'inserimento.");
				}
			}
		} while (!bin.equalsIgnoreCase("n"));

		//aggiunge i tipi di bevanda in input alle colonne del distributore
		for (Bevanda b : bevanda) {
			LinkedList<Bevanda> list = new LinkedList<>();
			list.add(b);
			distributore.getColonne().add(list);
		}

		// rifornimento delle colonne del distributore con il metodo aggiornaColonna()
		// di default è stato scelto di impostare la capacita' del distributore a 40 lattine per colonne
		for (LinkedList<Bevanda> list : distributore.getColonne()) {
			int colonna = distributore.getColonne().indexOf(list);
			String nomeBevanda = list.getFirst().getNome();
			distributore.aggiornaColonna(colonna, nomeBevanda, 40);
		}

		// tessere riconosciute dal distributore
		Tessera t0 = new Tessera(12);
		Tessera t1 = new Tessera(45);
		Tessera t2 = new Tessera(66);
		Tessera t3 = new Tessera(42);
		Tessera t4 = new Tessera(55);
		LinkedList<Tessera> tessera = new LinkedList<>();
		
		tessera.add(t0);
		tessera.add(t1);
		tessera.add(t2);
		tessera.add(t3);
		tessera.add(t4);
		
		// visualizza le bevande disponibili all'utente
		System.out.println("Bevande disponibili: ");
		for (LinkedList<Bevanda> col : distributore.getColonne()) {
			System.out.println(col.getFirst());
		}

		String in = "";
		System.out.println("Quale bevanda vuoi acquistare?");
		do {
			Tessera tesseraUtente = new Tessera();
			System.out.print("Inserisci il codice della bevanda o premere \"t\" per terminare: ");
			in = kb.next();
			Bevanda bev = new Bevanda();
			if (!in.equalsIgnoreCase("t")) {
				boolean validIn = false;
				try {
					distributore.infoBevanda(in);
					// prende la bevanda selezionata
					for (LinkedList<Bevanda> b : colonne) {
						if (in.equals(b.getFirst().getCodice())) {
							bev = b.getFirst();
							break;
						}
					}
					validIn = true;
				} catch (BevandaNonValidaException e) {
					System.out.println("Codice inserito non valido! Riprovare.");
				} 
				if (validIn) {
					boolean tesseraValida = false;
					//chiede il codice della tessera all'utente
					int tin = 0;
					System.out.print("Inserisci il codice della tua tessera: ");
					in = kb.next();
					if (Pattern.matches("[0-9]+", in)) {
						tin = Integer.parseInt(in); 	
						for (Tessera t : tessera) {
							if (t.getCodice() == tin) {
								tesseraUtente = t;
								tesseraValida = true;
							} 
						}
						if (tesseraValida == false) {
							System.out.println("Numero tessera non valido. Ripetere l'operazione...");
						}
					} else {
						System.out.println("Numero tessera non valido. Riprovare.");
					}
					if (tesseraValida) {
						// eroga la bevanda se il credito è almeno pari al
						// prezzo della bevanda
						if (tesseraUtente.getCreditoDisponibile() - bev.getPrezzo() > 0) {
							System.out.println("Erogazione in corso...");
							try {
								int colonna = distributore.eroga(bev.getCodice(), tesseraUtente.getCodice());
								tesseraUtente
										.setCreditoDisponibile(tesseraUtente.getCreditoDisponibile() - bev.getPrezzo());
								System.out.println("Prodotto erogato dalla colonna: " + colonna);
								System.out.println(
										"Lattine rimaste: " + distributore.lattineDisponibili(bev.getCodice()));
								System.out.println("Credito residuo sulla tessera: euro "
										+ tesseraUtente.leggiCredito(tesseraUtente.getCodice()));
							} catch (CreditoInsufficienteException e) {
								System.out.println("Credito insufficiente!");
							} catch (BevandaNonValidaException e) {
								System.out.println("Codice bevanda non valido! Riprovare.");
							}
							System.out.print("Vuoi acquistare un'altra bevanda? [y/n] ");
							in = kb.next();
							if (in.equalsIgnoreCase("n")) {
								in = "t";
								System.out.println("Operazione terminata.");
							}
						} else {
							//chiede all'utente se vuole ricaricare la tessera
							System.out.println("Credito insufficiente.");
							System.out.print("Vuoi ricaricare la tessera? [y/n] ");
							while (!in.equalsIgnoreCase("y") && !in.equalsIgnoreCase("n")) {
								in = kb.next();
								if (!in.equalsIgnoreCase("y") && !in.equalsIgnoreCase("n"))
									System.out.print("Inserimento non valido, rispondere solo con y o n: ");
								if (in.equalsIgnoreCase("n")) {
									System.out.println("Operazione terminata!");
									System.exit(0);
								}
							}
							System.out.println("Quanto vuoi ricaricare? ");
							System.out.print(
									"Inserisci un importo usando il punto come serparatore e due cifre decimali\n"
									+ "oppure inserire \"t\" per terminare. ");
							boolean caricata = false;
							while (!in.equalsIgnoreCase("t") && !caricata) {
								System.out.println("Inserisci l'importo: ");
								in = kb.next();
								if (Pattern.matches("[0-9]+.[0-9]{2}", in)) {
									if (Double.parseDouble(in) > bev.getPrezzo()) {
										tesseraUtente.caricaTessera(Double.parseDouble(in));
										System.out.println("Tessera ricaricata!");
										caricata = true;
									} else {
										System.out.println(
												"Tessera ricaricata, ma il credito rimane comunque insufficiente.");
										caricata = true;
									}
								} else if (in.equalsIgnoreCase("t")) {
									System.out.println("Operazione terminata.");
								} else {
									System.out.println("Inserimento non valido, riprovare.");
								}
							}
						}
					}
				}

			} else {
				System.out.println("Operazione terminata.");
			}

		} while (!in.equalsIgnoreCase("t"));

	}

}
