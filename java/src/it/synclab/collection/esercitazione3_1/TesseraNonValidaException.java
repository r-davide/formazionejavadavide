package it.synclab.collection.esercitazione3_1;

public class TesseraNonValidaException extends Exception {

	public TesseraNonValidaException() {
		super();
	}

	public TesseraNonValidaException(String message) {
		super(message);
	}
	
	
}
