package it.synclab.collection.esercitazione3_1;

public class BevandaNonValidaException extends Exception {

	public BevandaNonValidaException() {
		super();
	}

	public BevandaNonValidaException(String message) {
		super(message);
	}

}
