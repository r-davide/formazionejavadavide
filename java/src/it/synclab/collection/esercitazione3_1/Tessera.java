package it.synclab.collection.esercitazione3_1;

public class Tessera {

	private int codice;
	private double creditoDisponibile;
	
	public Tessera() {}

	public Tessera(int codice) {
		super();
		this.codice = codice;
		this.creditoDisponibile = 0.0;
	}

	public int getCodice() {
		return codice;
	}

	public double getCreditoDisponibile() {
		return creditoDisponibile;
	}

	public void setCreditoDisponibile(double creditoDisponibile) {
		this.creditoDisponibile = creditoDisponibile;
	}

	public double leggiCredito(int codice) throws TesseraNonValidaException {
		if (codice == this.codice)
			return this.creditoDisponibile;
		else
			throw new TesseraNonValidaException("ATTENZIONE: tessera non valida!");
	}

	public void caricaTessera(double importo) {
		setCreditoDisponibile(this.creditoDisponibile + importo);
	}

	public String toString() {
		return "Tessera [codice = " + codice + ", creditoDisponibile = " + creditoDisponibile + "]";
	}

}
