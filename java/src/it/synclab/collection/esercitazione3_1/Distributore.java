package it.synclab.collection.esercitazione3_1;

import java.util.LinkedList;

public class Distributore {

	private LinkedList<LinkedList<Bevanda>> colonne;

	public Distributore(LinkedList<LinkedList<Bevanda>> colonne) {
		super();
		this.colonne = colonne;
	}

	public LinkedList<LinkedList<Bevanda>> getColonne() {
		return colonne;
	}

	// se la bevanda con codice=codiceBevanda e' nel distributore, stampa nome e
	// prezzo
	public void infoBevanda(String codiceBevanda) throws BevandaNonValidaException {
		boolean trovato = false;
		Bevanda b = new Bevanda();
		for (LinkedList<Bevanda> list : colonne) {
			// ogni colonna ha un solo tipo di bevanda quindi prende la
			// prima
			b = list.getFirst();
			if (b.getCodice().equals(codiceBevanda)) {
				trovato = true;
				break;
			}
		}
		if (trovato) {
			System.out.println(b.getNome() + " - euro " + b.getPrezzo());
		} else {
			throw new BevandaNonValidaException("Codice selezionato non valido!");
		}
	}

	// aggiunge una bevanda alla volta nella colonna data come parametro
	public boolean aggiungiBevanda(int colonna, Bevanda b) {
		// utilizza indici per le colonne 0-4, mentre l'utente inserisce da 1-5
		boolean inserita = false;
		if (colonna < 0) {
			return false;
		}
		if (colonna < colonne.size()) {
			// se la colonna è vuota aggiunge la bevanda
			if (colonne.get(colonna).isEmpty()) {
				colonne.get(colonna).add(b);
				inserita = true;
			} else {
				// se non è vuota controlla che quelle contenute siano dello
				// stesso tipo di b
				// se sì aggiunge la bevanda, altrimenti no
				if (b.getCodice() == colonne.get(colonna).getFirst().getCodice()) {
					colonne.get(colonna).add(b);
					inserita = true;
				}
			}
		}
		return inserita;
	}

	// aggiorna la colonna passata come parametro riempiendola con la bevanda
	// corrispondente a "nomeBevanda"
	public boolean aggiornaColonna(int colonna, String nomeBevanda, int numLattine) {
		boolean ricaricata = false;
		if (colonna < 0) {
			return false;
		}
		// verifica che il valore della colonna inserita sia minore della
		// grandezza della lista
		if (colonna < colonne.size()) {
			// nome della bevanda contenuta nella colonna inserita come
			// parametro
			String b = colonne.get(colonna).getFirst().getNome();
			if (b.equalsIgnoreCase(nomeBevanda)) {
				Bevanda tmp = new Bevanda(colonne.get(colonna).getFirst().getCodice(), nomeBevanda,
						colonne.get(colonna).getFirst().getPrezzo());
				colonne.get(colonna).clear();
				for (int i = 0; i < numLattine; i++) {
					colonne.get(colonna).add(tmp);
				}
			}
		}

		return ricaricata;
	}

	public int lattineDisponibili(String codiceBevanda) throws BevandaNonValidaException {
		boolean trovato = false;
		LinkedList<Bevanda> tmp = new LinkedList<>();
		// verifica se è presente la bevanda con quel codice
		for (LinkedList<Bevanda> col : colonne) {
			if (col.getFirst().getCodice().equalsIgnoreCase(codiceBevanda)) {
				trovato = true;
				tmp = col;
			}
		}
		// se trova una colonna con la bevanda data in input ritona il numero di
		// bevande in quella colonna
		if (trovato) {
			return tmp.size();
		} else {
			throw new BevandaNonValidaException("Codice bevanda non valido!");
		}
	}

	public int eroga(String codBevanda, int codTessera)
			throws BevandaNonValidaException, CreditoInsufficienteException {
		boolean eroga = false;
		int colonna = 0;
		for (LinkedList<Bevanda> col : colonne) {
			if ((col.getFirst().getCodice() == codBevanda)) {
				eroga = true;
				colonna = colonne.indexOf(col);
				col.removeLast();
			}
		}
		if (eroga) {
			return colonna;
		} else {
			throw new BevandaNonValidaException("Codice bevanda non valido!");
		}
	}
}
