package it.synclab.collection.esercitazione3;

import java.util.LinkedList;

import javax.swing.text.html.HTMLDocument.Iterator;

public class Esercizio4 {

	/*
	 * Realizzare il metodo static LinkedList<LinkedList<Integer>
	 * insiemeDiInsiemi(int n) che costruisce una lista di liste così definita:
	 * a. Il primo elemento della lista contiene una lista con il solo valore 0;
	 * b. Il secondo elemento contiene una lista con gli elementi 0 e 1 c. Il
	 * terzo contiene una lista con gli elementi 0,1,2 d. … e così via fino ad
	 * n-1
	 */

	protected static LinkedList<LinkedList<Integer>> insiemeDiInsiemi(int n) {
		LinkedList<LinkedList<Integer>> listOfLists = new LinkedList<LinkedList<Integer>>();
		if (n < 1) {
			System.out.println("Inserire un valore in input maggiore di zero!");
			System.exit(0);
		} else {
			for (int i = 0; i < n; i++) {
				LinkedList list = new LinkedList<Integer>();
				listOfLists.add(list);
			}
			int j, i = 0;
			for (LinkedList<Integer> list : listOfLists) {
				j = i + 1;
				i = 0;
				while (i < j && j <= n) {
					list.add(i);
					i++;
				}
			}
		}
		return listOfLists;
	}

	/*
	 * Realizzare il metodo static void stampa(LinkedList <LinkedList<Integer>>
	 * a) in grado di stampare il contenuto della lista
	 */
	protected static void stampa(LinkedList<LinkedList<Integer>> a) {
		System.out.println("Insieme di insiemi:");
		for (LinkedList<Integer> l : a) {
			System.out.println(l);
		}
	}

	public static void main(String[] args) {

		LinkedList<LinkedList<Integer>> l = insiemeDiInsiemi(1);
		stampa(l);
	}

}
