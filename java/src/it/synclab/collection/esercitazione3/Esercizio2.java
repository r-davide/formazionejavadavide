package it.synclab.collection.esercitazione3;

import java.util.LinkedList;
import java.util.Iterator;
import java.util.Random;

public class Esercizio2 {

	/*
	 * Realizzare il metodo static LinkedList<Integer> creaRandomCrescente(int
	 * n) che genera una lista collegata costituita da n valori random
	 * crescenti.
	 */

	protected static LinkedList<Integer> creaRandomCrescente(int n) {
		LinkedList<Integer> list = new LinkedList<Integer>();
		if (n < 1) {
			System.out.println("creaRandomCrescente(): L'input deve essere maggiore di zero.");
		} else {
			Random r = new Random();
			list.add(r.nextInt(100));
			int current = list.getLast();
			for (int i = 0; i < n; ++i) {
				list.add(current + r.nextInt(100));
				current = list.getLast();
			}
		}
		return list;
	}

	/*
	 * Realizzare un metodo static LinkedList<Integer>
	 * parseString(LinkedList<String> a) che ritorna una lista Collegata di
	 * interi ottenuti applicando il metodo Integer.parseInt(…) agli elementi
	 * dell’iteratore passato come parametro.
	 * 
	 * Per il test generare una lista di stringhe opportuna con almeno 10
	 * elementi. Individuare almeno un input in cui il metodo genera una
	 * eccezione.
	 */

	protected static LinkedList<Integer> parseString(LinkedList<String> a) {
		LinkedList<Integer> list = new LinkedList<Integer>();
		Iterator<String> aIT = a.iterator();

		while (aIT.hasNext()) {
			list.add(Integer.parseInt((String) aIT.next()));
		}

		return list;
	}

	/*
	 * Realizzare il metodo static void provaEx2() per il test dei metodi
	 */
	protected static void provaEx2() {
		Random r = new Random();
		// LinkedList list1 = creaRandomCrescente(0);
		// System.out.print("Test metodo creaRandomCrescente():\n"
		// + "Contenuto list1 = ");
		// Esercizio1.stampaLL(list1);

		LinkedList<String> str = new LinkedList<>();
		// riempio la lista str con valori interi random
		for (int i = 0; i < 10; i++) {
			str.add(r.nextInt(100) + "");
		}
		// chiamo parseString()
		// non genera eccezione poiché l'input è corretto
		// LinkedList list2 = parseString(str);
		// System.out.print("Test metodo parseString(): \n" + "Contenuto list2 =
		// ");
		// Esercizio1.stampaLL(list2);

		// aggiungo alla lista str degli elementi che generano un'eccezione
		// di tipo NumberFormatException
		str.add("a");
		str.add("ciao");
		// richiamo il metodo parseString()
		try {
			LinkedList list3 = parseString(str);
			System.out.print("Test metodo parseString(): \n" + "Contenuto list3 = ");
			Esercizio1.stampaLL(list3);
		} catch (NumberFormatException ex) {
			System.out.println("NumberFormatException, \nlist3 = parseString(str): "
					+ "assicurarsi che str sia formata solo da interi.");
		}

	}

	public static void main(String[] args) {
		// TEST
		provaEx2();
	}

}
