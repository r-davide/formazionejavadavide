package it.synclab.collection.esercitazione3;

import java.util.LinkedList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public class Esercizio3 {

	/*
	 * Realizzare il metodo static LinkedList<Integer>
	 * mergeOrdinato(Iterator<Integer> a, Iterator<Integer> b) che effettua il
	 * merge ordinato degli elementi dei due iteratori, ritornando il risultato
	 * in una lista collegata. In particolare il merge di due liste ordinate
	 * (qui rappresentate dai corrispondenti iteratori, da assumere come già
	 * ordinati) restituisce una nuova lista ordinata contente tutti gli
	 * elementi appartenenti alle due liste di input.
	 */

	protected static LinkedList<Integer> mergeOrdinato(LinkedList<Integer> a, LinkedList<Integer> b) {
		LinkedList<Integer> merge = new LinkedList<Integer>();
		Iterator<Integer> bIT = b.iterator();
		Iterator<Integer> aIT = a.iterator();
		while (aIT.hasNext()) {
			merge.add(aIT.next());
		}
		while (bIT.hasNext()) {
			merge.add(bIT.next());
		}
		Collections.sort(merge);

		return merge;
	}

	/*
	 * Realizzare il metodo static void provaEx3() che crea due liste random
	 * ordinate e restituisce il merge ordinato delle due. Il risultato così
	 * ottenuto dovrà essere stampata, insieme ai corrispondenti vettori di
	 * input.
	 */

	protected static void provaEx3() {
		LinkedList<Integer> l1 = new LinkedList<>();
		LinkedList<Integer> l2 = new LinkedList<>();

		l1.add(6);
		l1.add(65);
		l1.add(3);

		l2.add(54);
		l2.add(3);
		l2.add(4);

		LinkedList<Integer> l = mergeOrdinato(l1, l2);
		System.out.println("Date le seguenti due liste: ");
		System.out.print("l1 = ");
		Esercizio1.stampaLL(l1);
		System.out.print("l2 = ");
		Esercizio1.stampaLL(l2);
		System.out.println();
		System.out.print("Chiama il metodo mergeOrdinato(l1, l2):\n" + "l = ");
		Esercizio1.stampaLL(l);
	}

	public static void main(String[] args) {

		provaEx3();
	}
}
