package it.synclab.collection.esercitazione3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class Esercizio1 {

	/*
	 * Realizzare il metodo static LinkedList<Integer> creaRandom(int n, int
	 * max) che genera una lista costituita da n valori interi random tra 0 e
	 * max-1.
	 * 
	 * ripetere gli esercizi utilizzando l’ArrayList al posto della LinkedList.
	 */

	protected static LinkedList<Integer> creaRandomLL(int n, int max) {
		LinkedList<Integer> listaRandom = new LinkedList<Integer>();
		if (n == 0 || max < 2) {
			System.out.println("Attenzione creaRandomLL():\nil primo parametro deve essere maggiore di zero,\n"
					+ "il secondo maggiore di 2.");
			System.exit(0);
		} else {
			Random r = new Random();
			for (int i = 0; i < n; i++) {
				listaRandom.add(r.nextInt(max - 1));
			}
		}

		return listaRandom;
	}

	protected static ArrayList<Integer> creaRandomAL(int n, int max) {
		ArrayList<Integer> listaRandom = new ArrayList<Integer>();
		if (n == 0 || max < 2) {
			System.out.println("Attenzione creaRandomAL():\nil primo parametro deve essere maggiore di zero,\n"
					+ "il secondo maggiore di 2.");
			System.exit(0);
		} else {
			Random r = new Random();
			for (int i = 0; i < n; i++) {
				listaRandom.add(r.nextInt(max - 1));
			}
		}

		return listaRandom;
	}

	/*
	 * Realizzare il metodo static void stampa(Iterator<Integer> i) che stampa
	 * gli elementi dell’iteratore nella forma <elem1>,<elem2>,…., <elemN>
	 * 
	 * ripetere gli esercizi utilizzando l’ArrayList al posto della LinkedList.
	 */
	protected static void stampaLL(LinkedList<Integer> list) {
		if (list.isEmpty()) {
			System.out.println("Lista vuota.");
		} else {
			Iterator<Integer> it = list.iterator();
			while (it.hasNext()) {
				System.out.print("<" + it.next() + "> ");
			}
			System.out.println();
		}
	}

	protected static void stampaAL(ArrayList<Integer> list) {
		if (list.isEmpty()) {
			System.out.println("Array vuoto.");
		} else {
			Iterator<Integer> it = list.iterator();
			while (it.hasNext()) {
				System.out.print("<" + it.next() + "> ");
			}
			System.out.println();
		}
	}

	/*
	 * Realizzare il metodo static void provaEx1() che, utilizzando i metodi
	 * appena creati, crei un vettore di 20 elementi random (sia ordinato che
	 * non) e li stampa. Questo metodo andrà poi chiamato dal main per i test di
	 * correttezza.
	 * 
	 * 
	 */
	protected static void provaEx1() {
		LinkedList<Integer> list = creaRandomLL(20, 50);
		System.out.println("LinkedList non ordinata: ");
		stampaLL(list);
		Collections.sort(list);
		System.out.println("\nLinkedList ordinata: ");
		stampaLL(list);

		ArrayList<Integer> list2 = creaRandomAL(20, 30);
		System.out.println("\nArrayList non ordinato:");
		stampaAL(list2);
		Collections.sort(list2);
		System.out.println("\nArrayList ordinata: ");
		stampaAL(list2);
	}

	public static void main(String[] args) {
		// TEST
		Esercizio1.provaEx1();
	}

}
